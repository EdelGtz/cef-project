﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace cef_backend.Data.Migrations
{
    public partial class CEFDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Energia",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Kilowatt = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Corriente = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Voltaje = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ConsumoTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ConsumoAcumulado = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    FactorPotencia = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Fecha = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DiaSemananumero = table.Column<int>(type: "int", nullable: false),
                    HoraPunta = table.Column<bool>(type: "bit", nullable: true),
                    Area = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Energia", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Energia");
        }
    }
}
