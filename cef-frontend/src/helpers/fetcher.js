
export const fetcher = async (url) => {
    const token = localStorage.getItem('token') || '';
    const headers = token
      ? {
          Authorization: "Bearer " + token,
        }
      : undefined;
  
    return fetch(process.env.REACT_APP_API_URL + url, {
      headers,
    }).then((r) => r.json());
  };
  