import Swal from "sweetalert2";
import { fetchConToken } from "../helpers/fetch";
import { types } from "../types/types";
import { finishLoading, startLoading } from "./ui";


/* DATOS PARA EL REPORTE DE CONSUMO DE ENERGIA*/
export const energiaStartLoading = ( fechaInicio, fechaFin, area ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`energia/${fechaInicio}/${fechaFin}/${area}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( energiaStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const energiaStartLoad = ( values ) => ({
    type: types.energiaTableStartLoad,
    payload: values
})

export const clearTableData = () => ({
    type: types.energiaClearTableData
})

/* DATOS PARA INDICADORES DE DASHBOARDENERGIASCREEN.JS*/
export const energiaIndicadoresStartLoading = ( fechaInicio, fechaFin ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`energia/indicadores/${fechaInicio}/${fechaFin}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( energiaIndicadoresStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally {
            dispatch( finishLoading() )
        }
    }
}

const energiaIndicadoresStartLoad = ( values ) => ({
    type: types.energiaIndicadoresStartLoad,
    payload: values
})

/* DATOS PARA GRAFICO DE FACTOR EE*/
export const energiaGraficaRendimientoEnergiaStartLoading = ( fechaInicio, fechaFin, area ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`energia/graficoRendimientoEnergia/${fechaInicio}/${fechaFin}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( energiaGraficaRendimientoEnergiaStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const energiaGraficaRendimientoEnergiaStartLoad = ( values ) => ({
    type: types.energiaGraficaRendimientoEnergia,
    payload: values
})

/* DATOS PARA GRAFICO DE CONSUMO DE ENERGIA */
export const energiaGraficoConsumoEnergiaStartLoading = ( fechaInicio, fechaFin, area ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`energia/graficoConsumoEnergia/${fechaInicio}/${fechaFin}/${area}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( energiaGraficoConsumoEnergiaStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const energiaGraficoConsumoEnergiaStartLoad = ( values ) => ({
    type: types.energiaGraficoConsumoEnergia,
    payload: values
})

/* DATOS PARA GRAFICO DE CONSUMO DE ENERGIA POR HORA */
export const energiaGraficoConsumoEnergiaHoraStartLoading = ( fechaInicio, fechaFin, area ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`energia/graficoConsumoEnergiaHora/${fechaInicio}/${fechaFin}/${area}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( energiaGraficoConsumoEnergiaHoraStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const energiaGraficoConsumoEnergiaHoraStartLoad = ( values ) => ({
    type: types.energiaGraficoConsumoEnergiaHora,
    payload: values
})

/* DATOS PARA GRAFICO DE PORCENTAJE DE CONSUMO DE ENERGIA */
export const energiaGraficoPorcenjateConsumoStartLoading = ( fechaInicio, fechaFin ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`energia/graficoPorcentajeConsumo/${fechaInicio}/${fechaFin}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( energiaGraficoPorcentajeConsumoStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const energiaGraficoPorcentajeConsumoStartLoad = ( values ) => ({
    type: types.energiaGraficoPorcentajeConsumo,
    payload: values
})