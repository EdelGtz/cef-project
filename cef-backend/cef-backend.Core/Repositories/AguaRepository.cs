﻿using cef_backend.Core.DTOs;
using cef_backend.Core.ExtensionMethods;
using cef_backend.Core.Interfaces;
using cef_backend.Data;
using Microsoft.EntityFrameworkCore;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.Repositories
{
    public class AguaRepository : IAgua
    {
        private readonly ApplicationDbContext context;

        public AguaRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public Task<List<AguaAreasDTO>> GetAreas()
        {
            var lst = new[] { new { Area = "--- TODAS ---" } }.ToList();
            var consulta = context.Agua.AsEnumerable().Select(s => new { Area = s.Area }).Distinct().ToList();

            lst.AddRange(consulta);

            return Task.FromResult(lst.Select(s => new AguaAreasDTO
            {
                Area = s.Area
            }).ToList());
        }

        public async Task<bool> GetDataFromGateway()
        {
            //Construir el request
            var client = new RestClient("http://10.21.4.61:1880/");
            var request = new RestRequest("/send-true", Method.POST);

            request.AddJsonBody(new
            {
                signal = true,
            });

            try
            {
                //Send request
                IRestResponse response = client.Execute(request);
                var content = response.Content; // {"message":" created."}

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoConsumoAgua(DateTime FechaInicio, DateTime FechaFin, string Area)
        {
            var query = await(
                                from e in context.Agua
                                where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date && (e.Area == Area)
                                select new
                                {
                                    fecha = e.Fecha.Value.Date,
                                    consumoTotal = e.ConsumoTotal
                                }
                            )
                            .GroupBy(g => new { g.fecha.Date })
                            .Select(s => new {
                                fecha = s.Key.Date,
                                consumo = s.Sum(t => t.consumoTotal)
                            })
                            .OrderBy(o => o.fecha.Date)
                            .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.fecha.Date.ToString("dd/MM/yyyy"),
                Valor = Decimal.Round((decimal)s.consumo, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoConsumoAgua(DateTime FechaInicio, DateTime FechaFin)
        {
            var query = await(
                                from e in context.Agua
                                where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                                select new
                                {
                                    fecha = e.Fecha.Value.Date,
                                    consumoTotal = e.ConsumoTotal
                                }
                            )
                            .GroupBy(g => new { g.fecha.Date })
                            .Select(s => new {
                                fecha = s.Key.Date,
                                consumo = s.Sum(t => t.consumoTotal)
                            })
                            .OrderBy(o => o.fecha.Date)
                            .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.fecha.Date.ToString("dd/MM/yyyy"),
                Valor = Decimal.Round((decimal)s.consumo, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoConsumoAguaHora(DateTime FechaInicio, DateTime FechaFin, string Area)
        {
            var query = await(
                            from e in context.Agua
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date && (e.Area == Area)
                            select new
                            {
                                hora = e.Fecha.Value.Hour,
                                consumoTotal = e.ConsumoTotal
                            }
                        )
                        .GroupBy(g => g.hora)
                        .Select(s => new {
                            hora = s.Key,
                            consumo = s.Sum(t => t.consumoTotal)
                        })
                        .OrderBy(o => o.hora)
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.hora.ToString(),
                Valor = Decimal.Round((decimal)s.consumo, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoConsumoAguaHora(DateTime FechaInicio, DateTime FechaFin)
        {            
            var query = await (
                            from e in context.Agua
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                            select new
                            {
                                hora = e.Fecha.Value.Hour,
                                consumoTotal = e.ConsumoTotal
                            }
                        )
                        .GroupBy(g => g.hora)
                        .Select(s => new {
                            hora = s.Key,
                            consumo = s.Sum(t => t.consumoTotal)
                        })
                        .OrderBy(o => o.hora)
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.hora.ToString(),
                Valor = Decimal.Round((decimal)s.consumo, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoRendimientoAgua(DateTime FechaInicio, DateTime FechaFin)
        {
            var query = await(
                            from e in context.Agua
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                            join p in context.Produccion
                            on e.Fecha.Value.Date equals p.Fecha.Date
                            select new
                            {
                                fecha = e.Fecha.Value.Date,
                                litrosBebida = p.LitrosBebida,
                                consumoTotal = e.ConsumoTotal
                            }
                        )
                        .GroupBy(g => new { g.fecha.Date, g.litrosBebida })
                        .Select(s => new {
                            fecha = s.Key.Date,
                            rendimiento = s.Key.litrosBebida / s.Sum(t => t.consumoTotal)
                        })
                        .OrderBy(o => o.fecha.Date)
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.fecha.Date.ToString("dd/MM/yyyy"),
                Valor = Decimal.Round((decimal)s.rendimiento, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoRendimientoAgua(DateTime FechaInicio, DateTime FechaFin, string Area)
        {
            var query = await(
                            from e in context.Agua
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date && e.Area == Area
                            join p in context.Produccion
                            on e.Fecha.Value.Date equals p.Fecha.Date
                            select new
                            {
                                fecha = e.Fecha.Value.Date,
                                litrosBebida = p.LitrosBebida,
                                consumoTotal = e.ConsumoTotal
                            }
                        )
                        .GroupBy(g => new { g.fecha.Date, g.litrosBebida })
                        .Select(s => new {
                            fecha = s.Key.Date,
                            rendimiento = s.Key.litrosBebida / s.Sum(t => t.consumoTotal)
                        })
                        .OrderBy(o => o.fecha.Date)
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.fecha.Date.ToString("dd/MM/yyyy"),
                Valor = Decimal.Round((decimal)s.rendimiento, 2)
            });

            return resultado;
        }

        public async Task<IndicadoresAguaDTO> GetIndicadores(DateTime FechaInicio, DateTime FechaFin)
        {
            var consumoAgua = await(from e in context.Agua
                                       where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                                       select new
                                       {
                                           totalEnergia = e.ConsumoTotal
                                       }
                                        )
                                        .SumAsync(s => s.totalEnergia);

            var cajasUnidad = await(from p in context.Produccion
                                    where p.Fecha.Date >= FechaInicio.Date && p.Fecha.Date <= FechaFin.Date
                                    select new
                                    {
                                        totalCajasUnidad = p.CajasUnidad
                                    }
                                        )
                                        .SumAsync(s => s.totalCajasUnidad);

            var consumoLitrosBebida = await(from p in context.Produccion
                                            where p.Fecha.Date >= FechaInicio.Date && p.Fecha.Date <= FechaFin.Date
                                            select new
                                            {
                                                totalLitrosBebida = p.LitrosBebida
                                            }
                                        )
                                        .SumAsync(s => s.totalLitrosBebida);

            var indicadorRendimientoAgua = consumoAgua * 1000 / consumoLitrosBebida;

            var indicadores = new IndicadoresAguaDTO
            {
                ConsumoAgua = Decimal.Round((decimal)consumoAgua, 2),
                CajasUnidad = Decimal.Round((decimal)cajasUnidad, 2),
                ConsumoLitrosBebida = Decimal.Round((decimal)consumoLitrosBebida, 2),
                IndicadorRendimientoAgua = Decimal.Round((decimal)indicadorRendimientoAgua, 2)
            };

            return indicadores;
        }

        public async Task<IEnumerable<GraficaDTO>> GetPorcenjateConsumo(DateTime FechaInicio, DateTime FechaFin)
        {
            var consumoTotal = await(
                 from e in context.Agua
                 where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                 select new
                 {
                     consumo = e.ConsumoTotal,
                 }
                ).SumAsync(s => s.consumo);

            var query = await(
                            from e in context.Agua
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                            select new
                            {
                                consumo = e.ConsumoTotal,
                                area = e.Area
                            }
                        )
                        .GroupBy(g => new { g.area })
                        .Select(s => new {
                            porcentaje = s.Sum(t => t.consumo) * 100 / consumoTotal,
                            area = s.Key.area
                        })
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.area,
                Valor = Decimal.Round((decimal)s.porcentaje, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<AguaDTO>> GetReporteAgua(DateTime FechaInicio, DateTime FechaFin)
        {
            var query = await (
                        from a in context.Agua
                        where a.Fecha.Value.Date >= FechaInicio.Date && a.Fecha.Value.Date <= FechaFin.Date
                        select new
                        {
                            fecha = a.Fecha.Value.Date,
                            area = a.Area,
                            consumoTotal = a.ConsumoTotal
                        }
                    )
                    .GroupBy(g => new { g.fecha, g.area })
                    .Select(s => new AguaDTO
                    {
                        Fecha = s.Key.fecha.Date.ToShortDateString(),
                        DiaSemana = s.Key.fecha.Date.ToString("dddd").CapitalizeFirst(),
                        Area = s.Key.area,
                        ConsumoTotal = Decimal.Round((decimal)s.Sum(sum => sum.consumoTotal), 2),
                    })
                    .ToListAsync();

            return query.OrderByDescending(o => o.Fecha);
        }

        public async Task<IEnumerable<AguaDTO>> GetReporteAgua(DateTime FechaInicio, DateTime FechaFin, string Area)
        {
            var query = await (
                        from a in context.Agua
                        where a.Fecha.Value.Date >= FechaInicio.Date && a.Fecha.Value.Date <= FechaFin.Date && (a.Area == Area)
                        select new
                        {
                            fecha = a.Fecha.Value.Date,
                            area = a.Area,
                            consumoTotal = a.ConsumoTotal
                        }
                    )
                    .GroupBy(g => new { g.fecha, g.area })
                    .Select(s => new AguaDTO
                    {
                        Fecha = s.Key.fecha.Date.ToShortDateString(),
                        DiaSemana = s.Key.fecha.Date.ToString("dddd").CapitalizeFirst(),
                        Area = s.Key.area,
                        ConsumoTotal = Decimal.Round((decimal)s.Sum(sum => sum.consumoTotal), 2),
                    })
                    .ToListAsync();

            return query.OrderByDescending(o => o.Fecha);
        }

        public async Task<IEnumerable<TotalizadoAguaDTO>> GetTotalizadoAgua(DateTime Fecha)
        {
            var query = await context.TotalizadoAgua.Where(w => w.FechaReporte.Value.Date == Fecha.Date)
                .Select(s => new {
                    Fecha = s.FechaReporte.Value.Date.ToString("dd/MM/yyyy"),
                    s.Area,
                    s.ConsumoTotal,
                    s.Orden
                })
                .OrderBy(o => o.Orden)
                .ToListAsync();

            return query.Select(s => new TotalizadoAguaDTO
            {
                Fecha = s.Fecha,
                Area = s.Area,
                ConsumoTotal = Decimal.Round((decimal)s.ConsumoTotal, 3),

            });
        }
    }
}
