import Swal from "sweetalert2";
import { fetchConToken } from "../helpers/fetch";
import { types } from "../types/types";
import { finishLoading, startLoading } from "./ui";


/* DATOS PARA EL REPORTE DE CONSUMO DE AGUA*/
export const aguaStartLoading = ( fechaInicio, fechaFin, area ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`agua/getReporteAgua/${fechaInicio}/${fechaFin}/${area}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( aguaStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const aguaStartLoad = ( values ) => ({
    type: types.aguaTableStartLoad,
    payload: values
})

export const clearTableData = () => ({
    type: types.aguaClearTableData
})

/* DATOS PARA INDICADORES DE DASHBOARDAGUASCREEN.JS*/
export const aguaIndicadoresStartLoading = ( fechaInicio, fechaFin ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`agua/indicadores/${fechaInicio}/${fechaFin}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( aguaIndicadoresStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally {
            dispatch( finishLoading() )
        }
    }
}

const aguaIndicadoresStartLoad = ( values ) => ({
    type: types.aguaIndicadoresStartLoad,
    payload: values
})

/* DATOS PARA GRAFICO DE RENDIMIENTO DE CONSUMO DE AGUA */
export const aguaGraficaRendimientoAguaStartLoading = ( fechaInicio, fechaFin, area ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`agua/getGraficoRendimientoAgua/${fechaInicio}/${fechaFin}/${area}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( aguaGraficaRendimientoAguaStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const aguaGraficaRendimientoAguaStartLoad = ( values ) => ({
    type: types.aguaGraficaRendimientoAgua,
    payload: values
})

/* DATOS PARA GRAFICO DE CONSUMO DE AGUA */
export const aguaGraficoConsumoAguaStartLoading = ( fechaInicio, fechaFin, area ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`agua/getGraficoConsumoAgua/${fechaInicio}/${fechaFin}/${area}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( aguaGraficoConsumoAguaStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const aguaGraficoConsumoAguaStartLoad = ( values ) => ({
    type: types.aguaGraficoConsumoAgua,
    payload: values
})

/* DATOS PARA GRAFICO DE CONSUMO DE AGUA POR HORA */
export const aguaGraficoConsumoAguaHoraStartLoading = ( fechaInicio, fechaFin, area ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`agua/getGraficoConsumoAguaHora/${fechaInicio}/${fechaFin}/${area}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( aguaGraficoConsumoAguaHoraStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const aguaGraficoConsumoAguaHoraStartLoad = ( values ) => ({
    type: types.aguaGraficoConsumoAguaHora,
    payload: values
})

/* DATOS PARA GRAFICO DE PORCENTAJE DE CONSUMO DE AGUA */
export const aguaGraficoPorcenjateConsumoStartLoading = ( fechaInicio, fechaFin ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`agua/getGraficoPorcentajeConsumo/${fechaInicio}/${fechaFin}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( aguaGraficoPorcentajeConsumoStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const aguaGraficoPorcentajeConsumoStartLoad = ( values ) => ({
    type: types.aguaGraficoPorcentajeConsumo,
    payload: values
})