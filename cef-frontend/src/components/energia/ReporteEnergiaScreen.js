import React, { useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import { Grid, IconButton } from '@mui/material';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { Filtros } from './Filtros';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { clearTableData } from '../../actions/energia';
import { excelExport } from '../../helpers/excelExport';
import moment from 'moment';
import { pdfExport } from '../../helpers/pdfExport';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        //backgroundColor: theme.palette.common.black,
        backgroundColor: '#202020',
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

let i = 1;

export const ReporteEnergiaScreen = () => {

    const { table } = useSelector(state => state.energia)
    const dispatch = useDispatch();
    const { pathname } = useLocation();

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleExcelExport = () => {
        let heading = [
            [
                "Fecha",
                "Dia Semana",
                "kW Avg",
                "Amperaje Avg",
                "Voltaje Avg",
                "f.d.p.",
                "Area",
                "Consumo Total Kw-h",
            ],
        ];
        let headers = [
            "fecha",
            "diaSemana",
            "kilowatt",
            "corriente",
            "voltaje",
            "factorPotencia",
            "area",
            "consumoTotal"
        ];
        excelExport(heading, headers, table, `REPORTE DE CONSUMO DE ENERGIA ${moment(Date.now()).format('DD-MM-YYYY')}`);
        setAnchorEl(null);
    }

    const handlePdfExport = () => {
        pdfExport(table, `REPORTE DE CONSUMO DE ENERGIA ${moment(Date.now()).format('DD-MM-YYYY')}`);
        setAnchorEl(null);
    }

    useEffect(() => {
        if (pathname === "/reporteConsumoEnergia") {
            dispatch(clearTableData());
        }
    }, [dispatch, pathname])

    return (
        <>
            <Grid container>
                <Grid item xs={10}>
                    <h3>Reporte de consumo de energía</h3>
                </Grid>
                <Grid item xs={2}>
                    <div style={{ marginTop: 15, float: "right" }}>
                        <IconButton
                            color="primary"
                            aria-label="FileDownload"
                            aria-controls="basic-menu"
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                            onClick={handleClick}
                        >
                            <FileDownloadIcon />
                        </IconButton>
                        <Menu
                            id="basic-menu"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            MenuListProps={{
                                'aria-labelledby': 'basic-button',
                            }}
                        >
                            <MenuItem onClick={handleExcelExport}>Excel (.xlsx)</MenuItem>
                            <MenuItem onClick={handlePdfExport}>PDF (.pdf)</MenuItem>
                        </Menu>
                    </div>
                </Grid>
            </Grid>

            <Filtros />

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 600 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Fecha</StyledTableCell>
                            <StyledTableCell align="center">Día de la semana</StyledTableCell>
                            <StyledTableCell align="right">kW Avg</StyledTableCell>
                            <StyledTableCell align="right">Amperaje Avg</StyledTableCell>
                            <StyledTableCell align="right">Voltaje Avg</StyledTableCell>
                            <StyledTableCell align="right">f.d.p. Avg</StyledTableCell>
                            <StyledTableCell align="center">Area</StyledTableCell>
                            <StyledTableCell align="right">Consumo Total kW-h</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {table.map((row) => (
                            <StyledTableRow key={i++}>
                                <StyledTableCell component="th" scope="row">
                                    {row.fecha}
                                </StyledTableCell>
                                <StyledTableCell align="center">{row.diaSemana}</StyledTableCell>
                                <StyledTableCell align="right">{row.kilowatt}</StyledTableCell>
                                <StyledTableCell align="right">{row.corriente}</StyledTableCell>
                                <StyledTableCell align="right">{row.voltaje}</StyledTableCell>
                                <StyledTableCell align="right">{row.factorPotencia}</StyledTableCell>
                                <StyledTableCell align="right">{row.area}</StyledTableCell>
                                <StyledTableCell align="right">{row.consumoTotal}</StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    )
}
