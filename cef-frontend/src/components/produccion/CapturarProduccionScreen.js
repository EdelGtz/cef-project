import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Grid, IconButton, Menu, MenuItem } from '@mui/material';
import { FiltrosCaptura } from './Filtros/FiltrosCaptura'
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import { clearTableData, produccionSetActive } from '../../actions/produccion';
import { useLocation } from 'react-router';
import AddIcon from '@mui/icons-material/Add';
import { DataGrid } from '@mui/x-data-grid';
import ModalAgregar from './ModalAgregar';

export const CapturarProduccionScreen = () => {

    const { table } = useSelector(state => state.produccion)
    const dispatch = useDispatch();
    const { pathname } = useLocation();

    const [selectionModel, setSelectionModel] = useState([]);

    const [openModal, setOpenModal] = useState(false);
    const handleOpen = () => setOpenModal(true);

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleEdit = (id) => {
        dispatch( produccionSetActive( id[0] ) )
        setOpenModal(true);
    }

    const handleClose = () => {
        setAnchorEl(null);
    };

    useEffect(() => {
        if (pathname === "/capturarProduccion") {
            dispatch(clearTableData());
        }
    }, [dispatch, pathname])
    
    return (
        <>
        <Grid container>
                <Grid item xs={7}>
                    <h3>Captura de producción de cajas unidad</h3>
                </Grid>
                <Grid item xs={4}>
                    <div style={{ marginTop: 15, float: "right" }}>
                        <Button onClick={ handleOpen } variant="contained" startIcon={<AddIcon />}>
                            Agregar nueva producción
                        </Button>
                    </div>
                </Grid>
                <Grid item xs={1}>
                    <div style={{ marginTop: 15, float: "right" }}>
                        <IconButton
                            color="primary"
                            aria-label="FileDownload"
                            aria-controls="basic-menu"
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                            onClick={handleClick}
                        >
                            <FileDownloadIcon />
                        </IconButton>
                        <Menu
                            id="basic-menu"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            MenuListProps={{
                                'aria-labelledby': 'basic-button',
                            }}
                        >
                            <MenuItem >Excel (.xlsx)</MenuItem>
                            <MenuItem >PDF (.pdf)</MenuItem>
                        </Menu>
                    </div>
                </Grid>
            </Grid>

            <FiltrosCaptura />

            <div style={{ height:'70vh', width: '100%' }}>
                <DataGrid 
                    rows={table}
                    columns={columns}
                    onSelectionModelChange={(selection) => {
                        if (selection.length > 1) {
                          const selectionSet = new Set(selectionModel);
                          const result = selection.filter((s) => !selectionSet.has(s));                    
                          setSelectionModel(result);
                          handleEdit(result);
                        } else {
                          setSelectionModel( selection );
                          handleEdit( selection );
                        }
                      }
                    }
                    selectionModel={ selectionModel }
                    { ...table }
                />
            </div>

            {/* <TableContainer component={Paper}>
                <Table sx={{ minWidth: 600 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Fecha</StyledTableCell>
                            <StyledTableCell align="left">Dia de la semana</StyledTableCell>
                            <StyledTableCell align="left">Cajas unidad</StyledTableCell>
                            <StyledTableCell align="left">Litros de bebida</StyledTableCell>                            
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {table.map((row) => (
                            <StyledTableRow key={row.id}>
                                <StyledTableCell component="th" scope="row" onClick={() => handleClick(console.log( row.id ) )}>
                                    {row.fecha}
                                </StyledTableCell>
                                <StyledTableCell align="left">{row.diaSemana}</StyledTableCell>
                                <StyledTableCell align="left">{row.cajasUnidad}</StyledTableCell>
                                <StyledTableCell align="left">{row.litrosBebida}</StyledTableCell>                                
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer> */}

            <ModalAgregar openModal={ openModal } setOpenModal={ setOpenModal } />
        </>
    )
}

const columns = [
    { field: 'id', headerName: 'ID', flex: 1 },
    {
      field: 'fecha',
      headerName: 'Fecha',
      type: 'date',
      flex: 1
    },
    { field: 'diaSemana', headerName: 'Dia de la semana', flex: 1 },
    { field: 'cajasUnidad', headerName: 'Cajas unidad', flex: 1 },
    { field: 'litrosBebida', headerName: 'Litros de bebida', flex: 1 },
  ];