﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Data.Models
{
    public class CO2
    {
        public int Id { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Flujo { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? ConsumoTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? ConsumoAcumulado { get; set; }
        [MaxLength(50)]
        public string? Turno { get; set; }
        public int? DiaSemanaNumero { get; set; }
        [MaxLength(50)]
        public string? Area { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
