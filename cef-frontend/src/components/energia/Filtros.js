import React, { useEffect, useState } from 'react'
import { Grid, IconButton } from '@mui/material'
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { es } from "date-fns/locale";
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { useTheme } from '@mui/material/styles';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import SearchIcon from '@mui/icons-material/Search';
import useSWR from 'swr';
import { useDispatch } from 'react-redux';
import { energiaGraficaRendimientoEnergiaStartLoading, energiaGraficoConsumoEnergiaHoraStartLoading, energiaGraficoConsumoEnergiaStartLoading, energiaGraficoPorcenjateConsumoStartLoading, energiaIndicadoresStartLoading, energiaStartLoading } from '../../actions/energia';
import moment from 'moment';
import Swal from 'sweetalert2';
import { useLocation } from 'react-router-dom';
import { produccionGraficoProduccionStartLoading } from '../../actions/produccion';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

function getStyles(name, areasName, theme) {
    return {
        fontWeight:
            areasName.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

export const Filtros = () => {

    const { data: areas } = useSWR('/energia/areas');

    const { pathname } = useLocation();

    const dispatch = useDispatch();

    const [fechaInicio, setFechaInicio] = useState(moment(Date.now()));
    const [fechaFin, setFechaFin] = useState(moment(Date.now()));

    const theme = useTheme();
    const [areasName, setAreasName] = useState('');

    useEffect(() => {
        //console.log('buscar')
        if (fechaInicio === null || fechaFin === null) {
            Swal.fire('Error', 'Debe seleccionar una fecha de inicio y una fecha final', 'error')
        } else {
            /* validar en que página está el componente actualmente para cargar los datos correspondientes */
            if (pathname === "/reporteConsumoEnergia" || pathname === "/" || pathname === "/*") {
                //console.log('reporteConsumoEnergia')
                dispatch( energiaStartLoading(
                        moment(fechaInicio).format('YYYY-MM-DD'),
                        moment(fechaFin).format('YYYY-MM-DD'),
                        areasName 
                    ));
            } else if(pathname === "/dashboardEnergia"){
                //console.log('dashboardEnergia')
                dispatch( energiaIndicadoresStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD')
                ));
                dispatch( energiaGraficaRendimientoEnergiaStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD'),
                ));
                dispatch( energiaGraficoConsumoEnergiaStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD'),
                    areasName
                ));
                dispatch( energiaGraficoConsumoEnergiaHoraStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD'),
                    areasName
                ));
                dispatch( produccionGraficoProduccionStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD'),
                    areasName
                ));
                dispatch( energiaGraficoPorcenjateConsumoStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD')
                ));
            }
        }
    }, [areasName, dispatch, fechaFin, fechaInicio, pathname])

    const handleChange = (event) => {
        setAreasName(event.target.value);
    };

    const handleSearch = () => {
        if (fechaInicio === null || fechaFin === null) {
            Swal.fire('Error', 'Debe seleccionar una fecha de inicio y una fecha final', 'error')
        } else {
            /* validar en que página está el componente actualmente para cargar los datos correspondientes */
            if (pathname === "/reporteConsumoEnergia" || pathname === "/" || pathname === "/*") {
                dispatch( energiaStartLoading(
                        moment(fechaInicio).format('YYYY-MM-DD'),
                        moment(fechaFin).format('YYYY-MM-DD'),
                        areasName 
                    ));
            } else if(pathname === "/dashboardEnergia"){
                dispatch( energiaIndicadoresStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD')
                ));
                dispatch( energiaGraficaRendimientoEnergiaStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD'),
                ));
                dispatch( energiaGraficoConsumoEnergiaStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD'),
                    areasName
                ));
                dispatch( energiaGraficoConsumoEnergiaHoraStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD'),
                    areasName
                ));
                dispatch( produccionGraficoProduccionStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD'),
                    areasName
                ));
                dispatch( energiaGraficoPorcenjateConsumoStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD')
                ));
            }
        }

    }

    if (!areas) {
        return <h1>Cargando...</h1>
      }

    return (
        <>
            <Grid container spacing={2} sx={{ mb: 1 }}>
                <Grid item >
                    <LocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
                        <DatePicker
                            label="Fecha Inicio"
                            value={fechaInicio}
                            onChange={(newValue) => {
                                setFechaInicio(newValue);
                            }}
                            renderInput={(params) => <TextField {...params} size="small" />}
                        />
                    </LocalizationProvider>
                </Grid>

                <Grid item>
                    <LocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
                        <DatePicker
                            label="Fecha Fin"
                            value={fechaFin}
                            onChange={(newValue) => {
                                setFechaFin(newValue);
                            }}
                            renderInput={(params) => <TextField {...params} size="small" />}
                        />
                    </LocalizationProvider>
                </Grid>

                <Grid item>
                    <FormControl sx={{ width: 250 }} size="small">
                        <InputLabel id="demo-multiple-name-label">Area</InputLabel>
                        <Select
                            labelId="demo-multiple-name-label"
                            id="demo-multiple-name"
                            value={areasName}
                            onChange={handleChange}
                            input={<OutlinedInput label="Area" />}
                            MenuProps={MenuProps}
                        >
                            {areas.map((area) => (
                                <MenuItem
                                    key={area.area}
                                    value={area.area}
                                    style={getStyles(area.area, areasName, theme)}
                                >
                                    {area.area}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item>
                    <IconButton 
                        aria-label="search"
                        sx={{
                            padding: 0, '& svg': {
                                fontSize: 30
                            }
                        }}
                        onClick={ handleSearch }
                    >
                        <SearchIcon />
                    </IconButton>
                </Grid>
            </Grid>
        </>
    )
}
