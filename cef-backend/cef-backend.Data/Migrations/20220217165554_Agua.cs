﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace cef_backend.Data.Migrations
{
    public partial class Agua : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Agua",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Flujo = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ConsumoTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ConsumoAcumulado = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Turno = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DiaSemanaNumero = table.Column<int>(type: "int", nullable: true),
                    Area = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Fecha = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Agua", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CO2",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Flujo = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ConsumoTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ConsumoAcumulado = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Turno = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DiaSemanaNumero = table.Column<int>(type: "int", nullable: true),
                    Area = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Fecha = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CO2", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Agua");

            migrationBuilder.DropTable(
                name: "CO2");
        }
    }
}
