﻿using cef_backend.Core.ExtensionMethods;
using cef_backend.Data;
using cef_backend.Core.DTOs;
using cef_backend.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cef_backend.Core.Interfaces;

namespace cef_backend.Controllers
{
    [Route("api/produccion")]
    [ApiController]
    public class ProduccionController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IProduccion produccion;

        public ProduccionController(ApplicationDbContext context, IProduccion produccion)
        {
            this.context = context;
            this.produccion = produccion;
        }

        [HttpGet("{FechaInicio}/{FechaFin}")]
        public async Task<ActionResult<ProduccionDTO>> Get(DateTime FechaInicio, DateTime FechaFin)
        {
            IEnumerable<ProduccionDTO> consulta = await produccion.GetProduccionData(FechaInicio, FechaFin);

            return Ok(consulta);
        }

        [HttpPost]
        public async Task<ActionResult<ProduccionAgregarDTO>> Post([FromBody] ProduccionAgregarDTO produccionAgregarDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existe= await context.Produccion.AnyAsync(a => a.Fecha.Date == produccionAgregarDTO.Fecha.Date);

            if (existe)
            {
                return BadRequest($"Ya existe un registro de producción con fecha {produccionAgregarDTO.Fecha.Date.ToString("dd/MM/yyyy")}");
            }

            bool response = await produccion.Agregar(produccionAgregarDTO);

            if (response)
            {
                return Ok("Registro agregado con exito");
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<ProduccionAgregarDTO>> Put(ProduccionAgregarDTO produccionAgregarDTO, int id)
        {
            var existe = await context.Produccion.AnyAsync(a => a.Id == id);

            if (!existe)
            {
                return NotFound("No se encontró el registro de esa producción");
            }

            bool response = await produccion.Editar(produccionAgregarDTO, id);

            if (response)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var existe = await context.Produccion.AnyAsync(a => a.Id == id);

            if (!existe)
            {
                return NotFound();
            }

            bool response = await produccion.Eliminar(id);

            if (response)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
