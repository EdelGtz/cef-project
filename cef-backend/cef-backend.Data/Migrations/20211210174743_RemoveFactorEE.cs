﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace cef_backend.Data.Migrations
{
    public partial class RemoveFactorEE : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FactorEE",
                table: "Produccion");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "FactorEE",
                table: "Produccion",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
