import React, { useEffect, useState } from "react";
import { Grid } from "@mui/material";
import ReactECharts from "echarts-for-react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Filtros } from "./Filtros";
import { useSelector } from "react-redux";

const initialStateGraficoRendimiento = {
  factorEE: [],
  dias: [],
};

const initialStateGraficoConsumo = {
  consumoKwh: [],
  diasConsumo: [],
};

const initialStateGraficoConsumoHora = {
  consumoPorHora: [],
  horas: [],
};

const initialStateGraficoProduccion = {
  produccion: [],
  diasProducidos: [],
};

const initialStateGraficoPorcenjate = {
  porcentaje:[],
  area:[],
};

export const DashboardAguaScreen = () => {
  const { indicadores, rendimiento, consumo, consumoHora, porcenjateConsumo } = useSelector(
    (state) => state.agua
  );
  const { graficoProduccion } = useSelector((state) => state.produccion);

  const [graficoRendimiento, setGraficoRendimiento] = useState(
    initialStateGraficoRendimiento
  );
  const [graficoConsumo, setGraficoConsumo] = useState(
    initialStateGraficoConsumo
  );
  const [graficoConsumoHora, setGraficoConsumoHora] = useState(
    initialStateGraficoConsumoHora
  );
  const [graficoProduccionBebida, setGraficoProduccionBebida] = useState(
    initialStateGraficoProduccion
  );
  const [graficoPorcenjate, setGraficoPorcentaje] = useState(
    initialStateGraficoPorcenjate
  );

  const { factorEE, dias } = graficoRendimiento;
  const { consumoKwh, diasConsumo } = graficoConsumo;
  const { consumoPorHora, horas } = graficoConsumoHora;
  const { produccion, diasProducidos } = graficoProduccionBebida;
  const { porcentaje, area } = graficoPorcenjate;

  useEffect(() => {
    setGraficoRendimiento({
      factorEE: rendimiento.map((x) => {
        return x.valor;
      }),
      dias: rendimiento.map((x) => {
        return x.etiqueta;
      }),
    });

    setGraficoConsumo({
      consumoKwh: consumo.map((x) => {
        return x.valor;
      }),
      diasConsumo: consumo.map((x) => {
        return x.etiqueta;
      }),
    });

    setGraficoConsumoHora({
      consumoPorHora: consumoHora.map((x) => {
        return x.valor;
      }),
      hora: consumoHora.map((x) => {
        return x.etiqueta;
      }),
    });

    setGraficoProduccionBebida({
      produccion: graficoProduccion.map((x) => {
        return x.valor;
      }),
      diasProducidos: graficoProduccion.map((x) => {
        return x.etiqueta;
      }),
    });

    setGraficoPorcentaje({
      porcentaje: porcenjateConsumo.map((x) => {
        return x.valor;
      }),
      area: porcenjateConsumo.map((x) => {
        return x.etiqueta;
      }),
    });
  }, [rendimiento, consumo, consumoHora, graficoProduccion, porcenjateConsumo]);

  let optionsGraficoRedimiento = {
    title: {
      text: "Indicador consumo de agua",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Factor EE"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: dias,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Indicador",
        type: "bar",
        data: factorEE,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
      {
        name: "Objetivo",
        type: "line",
        data: [1.65],
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
    ],
  };

  let optionsGraficoConsumo = {
    title: {
      text: "Consumo de agua en metros cúbicos",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Consumo de energía en kWh"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: diasConsumo,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Consumo de agua en m3",
        type: "bar",
        data: consumoKwh,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
    ],
  };

  let optionsGraficoConsumoHora = {
    title: {
      text: "Consumo de agua por hora",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Consumo por hora kWh"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: horas,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Consumo por hora",
        type: "line",
        data: consumoPorHora,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
    ],
  };

  let optionsGraficoProduccion = {
    title: {
      text: "Producción de litros de bebida por día",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Producción de litros de bebida"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: diasProducidos,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Producción de litros de bebida",
        type: "bar",
        data: produccion,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
    ],
  };

  let optionsGraficoPorcentaje = {
    title: {
      text: "Porcentaje de consumo por area",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Consumo de energía en kWh"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: area,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Porcentaje de consumo de agua",
        type: "bar",
        data: porcentaje,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        // markLine: {
        //   data: [{ type: "average", name: "Avg" }],
        // },
      },
    ],
  };

  return (
    <div>
      <h3>Análisis de consumo de agua</h3>

      <Filtros />

      <Grid
        container
        spacing={0}
        alignItems="center"
        justifyContent="center"
        sx={{ textAlign: "center" }}
      >
        <Grid item>
          <Card sx={{ minWidth: 160 }} elevation={0}>
            <CardContent>
              <Typography color="text.primary">Indicador consumo de agua</Typography>
              <Typography variant="h6" component="div">
                {indicadores != null ? indicadores.indicadorRendimientoAgua : 0}
              </Typography>
            </CardContent>
          </Card>
        </Grid>

        <Grid item>
          <Card sx={{ minWidth: 160 }} elevation={0}>
            <CardContent>
              <Typography color="text.primary">
                Consumo total de energía
              </Typography>
              <Typography variant="h6" component="div">
                {indicadores != null ? indicadores.consumoAgua : 0} kWh
              </Typography>
            </CardContent>
          </Card>
        </Grid>

        <Grid item>
          <Card sx={{ minWidth: 160 }} elevation={0}>
            <CardContent>
              <Typography color="text.primary">
                Total de cajas unidad
              </Typography>
              <Typography variant="h6" component="div">
                {indicadores != null ? indicadores.cajasUnidad : 0}
              </Typography>
            </CardContent>
          </Card>
        </Grid>

        <Grid item>
          <Card sx={{ minWidth: 160 }} elevation={0}>
            <CardContent>
              <Typography color="text.primary">
                Total de litros de bebida
              </Typography>
              <Typography variant="h6" component="div">
                {indicadores != null ? indicadores.consumoLitrosBebida : 0}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={6}>
          <ReactECharts
            option={optionsGraficoRedimiento}
            style={{ height: 400 }}
          />
        </Grid>

        <Grid item xs={6}>
          <ReactECharts
            option={optionsGraficoConsumo}
            style={{ height: 400 }}
          />
        </Grid>

        <Grid item xs={6}>
          <ReactECharts
            option={optionsGraficoProduccion}
            style={{ height: 400 }}
          />
        </Grid>

        <Grid item xs={6}>
          <ReactECharts
            option={optionsGraficoPorcentaje}
            style={{ height: 400 }}
          />
        </Grid>

        <Grid item xs={12}>
          <ReactECharts
            option={optionsGraficoConsumoHora}
            style={{ height: 400 }}
          />
        </Grid>
      </Grid>
    </div>
  );
}
