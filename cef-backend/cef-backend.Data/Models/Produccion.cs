﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Data.Models
{
    public class Produccion
    {
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CajasUnidad { get; set; }
        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal LitrosBebida { get; set; }
        public DateTime Fecha { get; set; }
    }
}
