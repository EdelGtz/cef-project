﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.DTOs
{
    public class IndicadoresEnergiaDTO
    {
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ConsumoEnergia { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CajasUnidad { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ConsumoLitrosBebida { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal IndicadorEE { get; set; }
    }
}
