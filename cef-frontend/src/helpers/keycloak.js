import Keycloak from "keycloak-js";

export const AUTH_URL = process.env.REACT_APP_AUTH_URL_KEYCLOAK;
//export const AUTH_URL = http://localhost:8080/auth;
//export const AUTH_URL = "http://192.168.0.36:8080/auth";
export const REALM = "cef";
export const CLIENT_ID = "cef-user-management";

const keycloak = Keycloak({
  url: AUTH_URL,
  realm: REALM,
  clientId: CLIENT_ID,
  clientSecret: "90f97e2a-5ec1-4de0-8d09-fffcb3869fbb"
});

export default keycloak;
