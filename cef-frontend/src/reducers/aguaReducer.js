import { types } from "../types/types";

const initialState = {
    table: [],
    indicadores: null,
    rendimiento: [],
    consumo: [],
    consumoHora: [],
    porcenjateConsumo: [],
}

export const aguaReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        case types.aguaTableStartLoad:
            return {
                ...state,
                table: [ ...action.payload ]
            }

        case types.aguaGraficaRendimientoAgua:
            return {
                ...state,
                rendimiento: [ ...action.payload ]
            }

        case types.aguaGraficoConsumoAgua:
            return {
                ...state,
                consumo: [ ...action.payload ]
            }

        case types.aguaGraficoConsumoAguaHora:
            return {
                ...state,
                consumoHora: [ ...action.payload ]
            }

        case types.aguaGraficoPorcentajeConsumo:
            return {
                ...state,
                porcenjateConsumo: [ ...action.payload ]
            }

        case types.aguaClearTableData:
            return {
                ...initialState
            }

        case types.aguaIndicadoresStartLoad:
            return {
                ...state,
                indicadores: action.payload
            }
    
        default:
            return state;
    }

}