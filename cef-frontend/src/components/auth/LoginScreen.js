import React, { useCallback } from 'react'
import { useKeycloak } from '@react-keycloak/web'
import { Navigate, useLocation } from "react-router-dom";
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { grey } from '@mui/material/colors';

import logo from '../../assets/logo-cef.svg';
import poweredBy from '../../assets/powered-by-eddno.svg';
import { styled } from '@mui/system';


function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" href="https://eddno.com/">
                EDDNO
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const theme = createTheme();

const LoginButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(grey[900]),
    backgroundColor: grey[900],
    '&:hover': {
      backgroundColor: grey[800],
    },
  }));

export const LoginScreen = () => {

    const location = useLocation();
    const currentLocationState = location.state || {
        from: { pathname: "/" },
    };

    const { keycloak } = useKeycloak();

    const login = useCallback(() => {
        keycloak?.login();
      }, [keycloak]);

    if (keycloak?.authenticated) return <Navigate to={currentLocationState} />;

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs" className="animate__animated animate__fadeIn animate_faster">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <img style={{ marginBottom: "10px", height:"35vh" }} src={logo} alt={"logo"} />
                    <Typography sx={{mt:2}} component="h1" variant="h5">
                        Iniciar Sesión
                    </Typography>
                    <Box sx={{ mt: 1 }}>
                        
                        <Box sx={{ m: 0, position: 'relative' }}>
                            <LoginButton
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2, backgroundColor: "#202020"}}
                                onClick={login}
                            >
                                Acceder
                            </LoginButton>
                        </Box>

                    </Box>
                </Box>
                <Copyright sx={{ mt: 2, mb: 2 }} />
                <div style={{textAlign: "right"}}>
                    <img style={{ maxWidth: "38%", height:"auto"}} src={poweredBy} alt={"poweredBy"} />
                </div>
            </Container>
        </ThemeProvider>
    )
}
