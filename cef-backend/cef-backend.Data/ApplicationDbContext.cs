﻿using cef_backend.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace cef_backend.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Energia> Energia { get; set; }
        public DbSet<Produccion> Produccion { get; set; }
        public DbSet<Agua> Agua { get; set; }
        public DbSet<TotalizadoAgua> TotalizadoAgua { get; set; }
        public DbSet<CO2> CO2 { get; set; }
    }
}
