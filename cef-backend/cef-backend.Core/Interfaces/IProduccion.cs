﻿using cef_backend.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.Interfaces
{
    public interface IProduccion
    {
        public Task<IEnumerable<ProduccionDTO>> GetProduccionData(DateTime FechaInicio, DateTime FechaFin);
        public Task<IEnumerable<GraficaDTO>> GetGraficoProduccion(DateTime FechaInicio, DateTime FechaFin);
        public Task<bool> Agregar(ProduccionAgregarDTO produccionAgregarDTO);
        public Task<bool> Editar(ProduccionAgregarDTO produccionAgregarDTO, int id);
        public Task<bool> Eliminar(int id);
    }
}
