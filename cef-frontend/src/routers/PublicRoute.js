import { Navigate } from 'react-router-dom';
import { useKeycloak } from '@react-keycloak/web';



export const PublicRoute = ({ children }) => {

    // const { uid } = useSelector(state => state.auth);
    // return !!uid

    const { keycloak, initialized } = useKeycloak();

    return initialized && keycloak?.authenticated
        ? <Navigate to="/*" />
        : children
}