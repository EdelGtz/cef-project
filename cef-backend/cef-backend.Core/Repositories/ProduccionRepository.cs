﻿using cef_backend.Core.DTOs;
using cef_backend.Core.ExtensionMethods;
using cef_backend.Core.Interfaces;
using cef_backend.Data;
using cef_backend.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.Repositories
{
    public class ProduccionRepository : IProduccion
    {
        private readonly ApplicationDbContext context;

        public ProduccionRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        public async Task<IEnumerable<ProduccionDTO>> GetProduccionData(DateTime FechaInicio, DateTime FechaFin)
        {
            var consulta = await context.Produccion.Where(w => w.Fecha.Date >= FechaInicio.Date && w.Fecha.Date <= FechaFin.Date).ToListAsync();

            return consulta.Select(s => new ProduccionDTO
            {
                Id = s.Id,
                Fecha = s.Fecha.Date.ToString("dd/MM/yyyy"),
                DiaSemana = s.Fecha.Date.ToString("dddd").CapitalizeFirst(),
                CajasUnidad = s.CajasUnidad,
                LitrosBebida = s.LitrosBebida,
            }).OrderByDescending(o => o.Fecha).ToList();
        }
        public async Task<IEnumerable<GraficaDTO>> GetGraficoProduccion(DateTime FechaInicio, DateTime FechaFin)
        {
            var query = await (
                            from e in context.Produccion
                            where e.Fecha.Date >= FechaInicio.Date && e.Fecha.Date <= FechaFin.Date
                            select new
                            {
                                fecha = e.Fecha.Date,
                                litrosBebida = e.LitrosBebida
                            }
                        )
                        .GroupBy(g => new { g.fecha.Date })
                        .Select(s => new {
                            fecha = s.Key.Date,
                            litrosBebida = s.Sum(t => t.litrosBebida)
                        })
                        .OrderBy(o => o.fecha.Date)
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.fecha.Date.ToString("dd/MM/yyyy"),
                Valor = Decimal.Round((decimal)s.litrosBebida, 2)
            });

            return resultado;
        }

        public async Task<bool> Agregar(ProduccionAgregarDTO produccionAgregarDTO)
        {
            Produccion produccion = new Produccion
            {
                CajasUnidad = produccionAgregarDTO.CajasUnidad,
                LitrosBebida = produccionAgregarDTO.CajasUnidad * Convert.ToDecimal(5.678),
                Fecha = produccionAgregarDTO.Fecha
            };

            context.Produccion.Add(produccion);

            try
            {
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Editar(ProduccionAgregarDTO produccionAgregarDTO, int id)
        {
            var produccion = await context.Produccion.FirstOrDefaultAsync(f => f.Id == id);

            produccion.CajasUnidad = produccionAgregarDTO.CajasUnidad;
            produccion.LitrosBebida = produccionAgregarDTO.CajasUnidad * Convert.ToDecimal(5.678);
            produccion.Fecha = produccion.Fecha;

            try
            {
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public async Task<bool> Eliminar(int id)
        {            
            try
            {
                context.Remove(new Produccion() { Id = id });
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }            
        }
    }
}
