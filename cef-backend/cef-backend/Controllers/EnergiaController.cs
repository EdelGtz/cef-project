﻿using cef_backend.Core.DTOs;
using cef_backend.Core.Interfaces;
using cef_backend.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cef_backend.Controllers
{
    [Route("api/energia")]
    [ApiController]
    public class EnergiaController : ControllerBase
    {
        private readonly IEnergia energia;
        private readonly IProduccion produccion;

        public EnergiaController(IEnergia energia, IProduccion produccion)
        {
            this.energia = energia;
            this.produccion = produccion;
        }

        [HttpGet("{FechaInicio}/{FechaFin}/{Area?}")]
        public async Task<ActionResult<EnergiaDTO>> Get(DateTime FechaInicio, DateTime FechaFin, string? Area = null)
        {
            if (string.IsNullOrEmpty(Area) || Area == "--- TODAS ---")
            {
                List<EnergiaDTO> resultado = await energia.GetEnergiaData(FechaInicio, FechaFin);
                return Ok(resultado);
            }
            else
            {
                List<EnergiaDTO> resultado = await energia.GetEnergiaData(FechaInicio, FechaFin, Area);
                return Ok(resultado);
            }
        }

        [HttpGet("areas")]
        public async Task<ActionResult<EnergiaAreasDTO>> GetAreas()
        {
            List<EnergiaAreasDTO> consulta = await energia.GetAreas();
            return Ok(consulta);
        }

        [HttpGet("graficoRendimientoEnergia/{FechaInicio}/{FechaFin}")]
        public async Task<ActionResult<GraficaDTO>> GetGraficoRendimientoEnergia(DateTime FechaInicio, DateTime FechaFin)
        {
            IEnumerable<GraficaDTO> resultado = await energia.GetGraficoRendimientoEnergia(FechaInicio, FechaFin);

            return Ok(resultado);
        }

        [HttpGet("graficoConsumoEnergia/{FechaInicio}/{FechaFin}/{Area?}")]
        public async Task<ActionResult<GraficaDTO>> GetGraficoConsumoEnergia(DateTime FechaInicio, DateTime FechaFin, string? Area = null)
        {
            if (string.IsNullOrEmpty(Area) || Area == "--- TODAS ---")
            {
                IEnumerable<GraficaDTO> resultado = await energia.GetGraficoConsumoEnergia(FechaInicio, FechaFin);

                return Ok(resultado);
            }
            else
            {
                IEnumerable<GraficaDTO> resultado = await energia.GetGraficoConsumoEnergia(FechaInicio, FechaFin, Area);

                return Ok(resultado);
            }
        }

        [HttpGet("graficoConsumoEnergiaHora/{FechaInicio}/{FechaFin}/{Area?}")]
        public async Task<ActionResult<GraficaDTO>> GetGraficoConsumoEnergiaHora(DateTime FechaInicio, DateTime FechaFin, string? Area = null)
        {
            if (string.IsNullOrEmpty(Area) || Area == "--- TODAS ---")
            {
                IEnumerable<GraficaDTO> resultado = await energia.GetGraficoConsumoEnergiaHora(FechaInicio, FechaFin);

                return Ok(resultado);
            }
            else
            {
                IEnumerable<GraficaDTO> resultado = await energia.GetGraficoConsumoEnergiaHora(FechaInicio, FechaFin, Area);

                return Ok(resultado);
            }
            
        }

        [HttpGet("graficoProduccion/{FechaInicio}/{FechaFin}")]
        public async Task<ActionResult<GraficaDTO>> GetGraficoProduccion(DateTime FechaInicio, DateTime FechaFin)
        {
            IEnumerable<GraficaDTO> resultado = await produccion.GetGraficoProduccion(FechaInicio, FechaFin);

            return Ok(resultado);
        }

        [HttpGet("graficoPorcentajeConsumo/{FechaInicio}/{FechaFin}")]
        public async Task<ActionResult<GraficaDTO>> GetPorcenjateConsumo(DateTime FechaInicio, DateTime FechaFin)
        {
            IEnumerable<GraficaDTO> resultado = await energia.GetPorcenjateConsumo(FechaInicio, FechaFin);

            return Ok(resultado);
        }

        [HttpGet("indicadores/{FechaInicio}/{FechaFin}")]
        public async Task<ActionResult<IndicadoresEnergiaDTO>> GetIndicadores(DateTime FechaInicio, DateTime FechaFin)
        {
            var indicadores = await energia.GetIndicadores(FechaInicio, FechaFin);

            return Ok(indicadores);
        }
    }
}
