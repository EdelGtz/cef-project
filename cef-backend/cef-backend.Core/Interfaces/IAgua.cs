﻿using cef_backend.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.Interfaces
{
    public interface IAgua
    {
        public Task<List<AguaAreasDTO>> GetAreas();
        public Task<IEnumerable<AguaDTO>> GetReporteAgua(DateTime FechaInicio, DateTime FechaFin);
        public Task<IEnumerable<AguaDTO>> GetReporteAgua(DateTime FechaInicio, DateTime FechaFin, string Area);
        public Task<IEnumerable<TotalizadoAguaDTO>> GetTotalizadoAgua(DateTime Fecha);
        public Task<IEnumerable<GraficaDTO>> GetGraficoRendimientoAgua(DateTime FechaInicio, DateTime FechaFin);
        public Task<IEnumerable<GraficaDTO>> GetGraficoRendimientoAgua(DateTime FechaInicio, DateTime FechaFin, string Area);
        public Task<IEnumerable<GraficaDTO>> GetGraficoConsumoAgua(DateTime FechaInicio, DateTime FechaFin, string Area);
        public Task<IEnumerable<GraficaDTO>> GetGraficoConsumoAgua(DateTime FechaInicio, DateTime FechaFin);
        public Task<IEnumerable<GraficaDTO>> GetGraficoConsumoAguaHora(DateTime FechaInicio, DateTime FechaFin, string Area);
        public Task<IEnumerable<GraficaDTO>> GetGraficoConsumoAguaHora(DateTime FechaInicio, DateTime FechaFin);
        public Task<IEnumerable<GraficaDTO>> GetPorcenjateConsumo(DateTime FechaInicio, DateTime FechaFin);
        public Task<IndicadoresAguaDTO> GetIndicadores(DateTime FechaInicio, DateTime FechaFin);
        public Task<bool> GetDataFromGateway();
    }
}
