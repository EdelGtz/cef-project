import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk';

import { authReducer } from '../reducers/authReducer';
import { energiaReducer } from '../reducers/energiaReducer';
import { aguaReducer } from '../reducers/aguaReducer';
import { uiReducer } from '../reducers/uiReducer';
import { prodReducer } from '../reducers/prodReducer';

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducers = combineReducers({
    auth: authReducer,
    ui: uiReducer,
    energia: energiaReducer,
    agua: aguaReducer,
    produccion: prodReducer,
})

export const store = createStore(
    reducers,
    composeEnhancers(
        applyMiddleware( thunk )
    )
);