import React, { useState } from 'react'
import { Link, useLocation } from 'react-router-dom';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import WaterIcon from '@mui/icons-material/Water';
import OutletIcon from '@mui/icons-material/Outlet';
import DescriptionIcon from '@mui/icons-material/Description';
import EqualizerIcon from '@mui/icons-material/Equalizer';
//import OndemandVideoIcon from '@mui/icons-material/OndemandVideo';
import PrecisionManufacturingIcon from '@mui/icons-material/PrecisionManufacturing';
import AddBusinessIcon from '@mui/icons-material/AddBusiness';

import logo from '../../assets/logo-navbar-cef.svg';
import '../../styles/Sidebar.css';
import { styled } from '@mui/system';

const StyledList = styled(List)({
  // selected and (selected + hover) states
  '&& .Mui-selected, && .Mui-selected:hover': {
    backgroundColor: '#ffd9da',
    '&, & .MuiListItemIcon-root': {
      //color: 'black',
    },
  },
  // hover states
  '& .MuiListItemButton-root:hover': {
    backgroundColor: '#e8e8e8',
    '&, & .MuiListItemIcon-root': {
      //color: 'yellow',
    },
  },
});

export const Sidebar = ({ open, handleDrawerClose, theme, DrawerHeader, drawerWidth }) => {

  const { pathname } = useLocation();

  const [openListAgua, setOpenListAgua] = useState(false);
  const handleClickAgua = () => {
    setOpenListAgua(!openListAgua);
  };

  // const [openListCo2, setOpenListCo2] = useState(false);
  // const handleClickCo2 = () => {
  //   setOpenListCo2(!openListCo2);
  // };

  const [openListEnergia, setOpenListEnergia] = useState(false);
  const handleClickEnergia = () => {
    setOpenListEnergia(!openListEnergia);
  };

  const [openListProduccion, setOpenListProduccion] = useState(false);
  const handleClickProduccion = () => {
    setOpenListProduccion(!openListProduccion);
  };

  return (
    <Drawer
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        '& .MuiDrawer-paper': {
          width: drawerWidth,
          boxSizing: 'border-box',
        },
      }}
      variant="persistent"
      anchor="left"
      open={open}
    >
      <DrawerHeader>
        <Box sx={{ display: 'flex', mt:1, mr:2 }}>
          <Link to="/">
            <img width="150" src={logo} alt={"logo"} />
          </Link>
        </Box>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
        </IconButton>
      </DrawerHeader>

      {/* <Divider /> */}

      <StyledList
        sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper', }}
        component="nav"
        aria-labelledby="nested-list-subheader"
      >

        <ListItemButton onClick={handleClickAgua}>
          <ListItemIcon>
            <WaterIcon />
          </ListItemIcon>
          <ListItemText primary="Agua" />
          {openListAgua ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openListAgua} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>

            <ListItemButton
              sx={{ pl: 4 }}
              component={Link}
              to="/dashboardAgua"
              button
              selected={pathname === "/dashboardAgua"}
            >
              <ListItemIcon>
                <EqualizerIcon />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItemButton>

            <ListItemButton 
              sx={{ pl: 4 }}
              component={Link}
              to="/reporteConsumoAgua"
              button
              selected={pathname === "/reporteConsumoAgua"}
            >
              <ListItemIcon>
                <DescriptionIcon />
              </ListItemIcon>
              <ListItemText primary="Reporte de consumo" />
            </ListItemButton>

            <ListItemButton 
              sx={{ pl: 4 }}
              component={Link}
              to="/reporteConsumoAguaTotalizado"
              button
              selected={pathname === "/reporteConsumoAguaTotalizado"}
            >
              <ListItemIcon>
                <DescriptionIcon />
              </ListItemIcon>
              <ListItemText primary="Reporte de consumo totalizado" />
            </ListItemButton>

          </List>
        </Collapse>

        {/* <ListItemButton onClick={handleClickCo2}>
          <ListItemIcon>
            <PanoramaWideAngleSelectIcon />
          </ListItemIcon>
          <ListItemText primary="CO2" />
          {openListCo2 ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openListCo2} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemButton sx={{ pl: 4 }}>
              <ListItemIcon>
                <AccessTimeIcon />
              </ListItemIcon>
              <ListItemText primary="Próximamente..." />
            </ListItemButton>
          </List>
        </Collapse> */}

        <ListItemButton onClick={handleClickEnergia}>
          <ListItemIcon>
            <OutletIcon />
          </ListItemIcon>
          <ListItemText primary="Energia" />
          {openListEnergia ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openListEnergia} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {/* <ListItemButton 
              sx={{ pl: 4 }}
              component={Link}
              to="/monitoreoEnergia"
              button
              selected={pathname === "/monitoreoEnergia"}
            >
              <ListItemIcon>
                <OndemandVideoIcon />
              </ListItemIcon>
              <ListItemText primary="Monitoreo" />
            </ListItemButton> */}

            <ListItemButton 
              sx={{ pl: 4 }}
              component={Link}
              to="/dashboardEnergia"
              button
              selected={pathname === "/dashboardEnergia"}
            >
              <ListItemIcon>
                <EqualizerIcon />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItemButton>

            <ListItemButton 
              sx={{ pl: 4 }}
              component={Link}
              to="/reporteConsumoEnergia"
              button
              selected={pathname === "/reporteConsumoEnergia"}
            >
              <ListItemIcon>
                <DescriptionIcon />
              </ListItemIcon>
              <ListItemText primary="Reporte de consumo" />
            </ListItemButton>
          </List>
        </Collapse>

        <ListItemButton onClick={handleClickProduccion}>
          <ListItemIcon>
            <PrecisionManufacturingIcon />
          </ListItemIcon>
          <ListItemText primary="Producción" />
          {openListProduccion ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={openListProduccion} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemButton
              sx={{ pl: 4 }}
              component={Link}
              to="/capturarProduccion"
              button
              selected={pathname === "/capturarProduccion"}
            >
              <ListItemIcon>
                <AddBusinessIcon />
              </ListItemIcon>
              <ListItemText primary="Capturar producción" />
            </ListItemButton>
          </List>
        </Collapse>

      </StyledList>
    </Drawer>
  )
}

