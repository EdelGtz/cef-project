import React from 'react'
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { CEFRoutes } from './CEFRoutes';
import { LoginScreen } from '../components/auth/LoginScreen';
import { PublicRoute } from './PublicRoute';
import { PrivateRoute } from './PrivateRoute';
//import { startLoadingNotes } from '../actions/notes';

export const AppRouter = () => {

    // const dispatch = useDispatch();
    // const { checking } = useSelector(state => state.auth);

    // useEffect(() => {
    //     console.log('checando')
    //     dispatch(startChecking());
    // }, [dispatch])

    // if (checking) {
    //     return (
    //         // <h1>Wait...</h1>
    //         <Backdrop
    //             sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
    //             open={checking}
    //         >
    //             <CircularProgress color="inherit" />
    //         </Backdrop>
    //     )
    // }

    return (
        <BrowserRouter>
            <div>
                <Routes>

                    <Route path="/auth/login" element={
                        <PublicRoute>
                            <LoginScreen />
                        </PublicRoute>
                    }
                    />

                    <Route path="/*" element={
                        <PrivateRoute>
                            <CEFRoutes />
                        </PrivateRoute>
                    }
                    />

                    {/* <Route element={<Navigate to="/" />} /> */}
                </Routes>
            </div>
        </BrowserRouter>
    )
}
