import React, { useState } from 'react';
import { Routes, Route } from "react-router-dom"
import { styled, useTheme } from '@mui/material/styles';
import MuiAppBar from '@mui/material/AppBar';

import CssBaseline from '@mui/material/CssBaseline';

import { Sidebar } from '../components/main/Sidebar';
import { MainAppBar } from '../components/main/AppBar';
import { ReporteEnergiaScreen } from '../components/energia/ReporteEnergiaScreen';
import { DashboardEnergiaScreen } from '../components/energia/DashboardEnergiaScreen';
import { MonitoreoEnergiaScreen } from '../components/energia/MonitoreoEnergiaScreen';
import { DashboardAguaScreen } from '../components/agua/DashboardAguaScreen';
import { ReporteAguaScreen } from '../components/agua/ReporteAguaScreen';
import { Box } from '@mui/system';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { useSelector } from 'react-redux';
import { CapturarProduccionScreen } from '../components/produccion/CapturarProduccionScreen';
import { ReporteAguaTotalizadoScreen } from '../components/agua/ReporteAguaTotalizadoScreen';


const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

export const CEFRoutes = () => {
    const theme = useTheme();
    const [open, setOpen] = useState(true);

    const {loading} = useSelector(state => state.ui)

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Main open={open}>
                <Box sx={{ display: 'flex' }}>
                    {/* <DrawerHeader /> */}

                    <CssBaseline />

                    <Backdrop
                        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                        open={loading}
                    >
                        <CircularProgress color="inherit" />
                    </Backdrop>

                    <MainAppBar open={open} handleDrawerOpen={handleDrawerOpen} AppBar={AppBar} />

                    <Sidebar open={open} handleDrawerClose={handleDrawerClose} theme={theme} DrawerHeader={DrawerHeader} drawerWidth={drawerWidth} />

                    <div style={{ width: "100%", marginTop:32 }}>
                        <Routes>
                            {/* Energia */}
                            <Route exact path="/monitoreoEnergia" element={<MonitoreoEnergiaScreen />} />
                            <Route exact path="/dashboardEnergia" element={<DashboardEnergiaScreen />} />
                            <Route exact path="/reporteConsumoEnergia" element={<ReporteEnergiaScreen />} />
                            {/* Produccion */}
                            <Route exact path="/capturarProduccion" element={<CapturarProduccionScreen />} />
                            {/* Agua */}
                            <Route exact path="/dashboardAgua" element={<DashboardAguaScreen />} />
                            <Route exact path="/reporteConsumoAgua" element={<ReporteAguaScreen />} />
                            <Route exact path="/reporteConsumoAguaTotalizado" element={<ReporteAguaTotalizadoScreen />} />
                            {/* Default */}
                            <Route path="/*" element={<ReporteEnergiaScreen />} />
                        </Routes>
                    </div>

                </Box>
            </Main>
        </>
    );
}