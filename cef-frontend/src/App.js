import React from 'react'
import { Provider } from 'react-redux'
import { SWRConfig } from 'swr'

import { store } from './store/store'
import { AppRouter } from './routers/AppRouter'
import { fetcher } from './helpers/fetcher'

export const App = () => {
    return (
        <Provider store={ store }>
            <SWRConfig
                value={{ fetcher: (url) => fetcher(url), revalidateOnReconnect: true }}
            >
                <AppRouter />
            </SWRConfig>
        </Provider>
    )
}
