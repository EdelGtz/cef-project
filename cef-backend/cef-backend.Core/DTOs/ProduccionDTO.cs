﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.DTOs
{
    public class ProduccionDTO
    {
        public int Id { get; set; }
        public string Fecha { get; set; }
        public string DiaSemana { get; set; }
        public decimal CajasUnidad { get; set; }
        public decimal LitrosBebida { get; set; }
    }
}
