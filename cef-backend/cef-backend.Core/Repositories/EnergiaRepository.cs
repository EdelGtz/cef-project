﻿using cef_backend.Core.Interfaces;
using cef_backend.Data;
using cef_backend.Core.DTOs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using cef_backend.Core.ExtensionMethods;

namespace cef_backend.Core.Repositories
{
    public class EnergiaRepository : IEnergia
    {
        private readonly ApplicationDbContext context;

        public EnergiaRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public Task<List<EnergiaDTO>> GetEnergiaData(DateTime FechaInicio, DateTime FechaFin, string? Area = null)
        {
            var consulta = context.Energia.Where(w => (w.Fecha.Value.Date >= FechaInicio.Date && w.Fecha.Value.Date <= FechaFin.Date) && (w.Area == Area))
                .AsEnumerable()
                .GroupBy(g => new { fecha = g.Fecha.Value.Date.ToString("dd/MM/yyyy"), diaSemana = g.Fecha.Value.Date.ToString("dddd"), area = g.Area })
                .Select(s => new
                {
                    Fecha = s.Key.fecha,
                    DiaSemana = s.Key.diaSemana,
                    KwAvg = s.Where(w => w.Kilowatt != 0).Average(a => a.Kilowatt) == null ? 0 : s.Where(w => w.Kilowatt != 0).Average(a => a.Kilowatt),
                    CorrienteAvg = s.Where(w => w.Corriente != 0).Average(a => a.Corriente) == null ? 0 : s.Where(w => w.Corriente != 0).Average(a => a.Corriente),
                    VoltajeAvg = s.Where(w => w.Voltaje != 0).Average(a => a.Voltaje) == null ? 0 : s.Where(w => w.Voltaje != 0).Average(a => a.Voltaje),
                    FactorPotencia = s.Where(w => w.FactorPotencia != 0).Average(a => a.FactorPotencia) == null ? 0 : s.Where(w => w.FactorPotencia != 0).Average(a => a.FactorPotencia),
                    Area = s.Key.area,
                    ConsumoTotal = s.Sum(a => a.ConsumoTotal)
                })
                .OrderByDescending(o => Convert.ToDateTime(o.Fecha).Date)
                .ToList();

            return Task.FromResult(consulta.Select(s => new EnergiaDTO
            {
                Fecha = s.Fecha,
                DiaSemana = s.DiaSemana.ToString().CapitalizeFirst(),
                Kilowatt = Decimal.Round((decimal)s.KwAvg, 2),
                Corriente = Decimal.Round((decimal)s.CorrienteAvg, 2),
                Voltaje = Decimal.Round((decimal)s.VoltajeAvg, 2),
                FactorPotencia = s.FactorPotencia == null ? 0 : Decimal.Round((decimal)s.FactorPotencia, 2),
                Area = s.Area,
                ConsumoTotal = Decimal.Round((decimal)s.ConsumoTotal, 2)
            }).ToList());
        }

        public Task<List<EnergiaDTO>> GetEnergiaData(DateTime FechaInicio, DateTime FechaFin)
        {
            var consulta = context.Energia.Where(w => (w.Fecha.Value.Date >= FechaInicio.Date && w.Fecha.Value.Date <= FechaFin.Date))
                .AsEnumerable()
                .GroupBy(g => new { fecha = g.Fecha.Value.Date.ToString("dd/MM/yyyy"), diaSemana = g.Fecha.Value.Date.ToString("dddd"), area = g.Area })
                .Select(s => new
                {
                    Fecha = s.Key.fecha,
                    DiaSemana = s.Key.diaSemana,
                    KwAvg = s.Where(w => w.Kilowatt != 0).Average(a => a.Kilowatt) == null ? 0 : s.Where(w => w.Kilowatt != 0).Average(a => a.Kilowatt),
                    CorrienteAvg = s.Where(w => w.Corriente != 0).Average(a => a.Corriente) == null ? 0 : s.Where(w => w.Corriente != 0).Average(a => a.Corriente),
                    VoltajeAvg = s.Where(w => w.Voltaje != 0).Average(a => a.Voltaje) == null ? 0 : s.Where(w => w.Voltaje != 0).Average(a => a.Voltaje),
                    FactorPotencia = s.Where(w => w.FactorPotencia != 0).Average(a => a.FactorPotencia) == null ? 0 : s.Where(w => w.FactorPotencia != 0).Average(a => a.FactorPotencia),
                    Area = s.Key.area,
                    ConsumoTotal = s.Sum(a => a.ConsumoTotal)
                })
                .OrderByDescending(o => Convert.ToDateTime(o.Fecha).Date)
                .ToList();

            return Task.FromResult(consulta.Select(s => new EnergiaDTO
            {
                Fecha = s.Fecha,
                DiaSemana = s.DiaSemana.ToString().CapitalizeFirst(),
                Kilowatt = Decimal.Round((decimal)s.KwAvg, 2),
                Corriente = Decimal.Round((decimal)s.CorrienteAvg, 2),
                Voltaje = Decimal.Round((decimal)s.VoltajeAvg, 2),
                FactorPotencia = s.FactorPotencia == null ? 0 : Decimal.Round((decimal)s.FactorPotencia, 2),
                Area = s.Area,
                ConsumoTotal = Decimal.Round((decimal)s.ConsumoTotal, 2)
            }).ToList());
        }

        public Task<List<EnergiaAreasDTO>> GetAreas()
        {
            var lst = new[] { new { Area = "--- TODAS ---"} }.ToList();
            var consulta = context.Energia.AsEnumerable().Select(s => new { Area = s.Area }).Distinct().ToList();

            lst.AddRange(consulta);

            return Task.FromResult(lst.Select(s => new EnergiaAreasDTO
            {
                Area = s.Area
            }).ToList());
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoRendimientoEnergia(DateTime FechaInicio, DateTime FechaFin)
        {
            var query = await(
                            from e in context.Energia
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                            join p in context.Produccion
                            on e.Fecha.Value.Date equals p.Fecha.Date
                            select new
                            {
                                fecha = e.Fecha.Value.Date,
                                litrosBebida = p.LitrosBebida,
                                consumoTotal = e.ConsumoTotal
                            }
                        )
                        .GroupBy(g => new { g.fecha.Date, g.litrosBebida })
                        .Select(s => new {
                            fecha = s.Key.Date,
                            rendimiento = s.Key.litrosBebida / s.Sum(t => t.consumoTotal)
                        })
                        .OrderBy(o => o.fecha.Date)
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.fecha.Date.ToString("dd/MM/yyyy"),
                Valor = Decimal.Round((decimal)s.rendimiento, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoConsumoEnergia(DateTime FechaInicio, DateTime FechaFin, string Area)
        {
            var query = await (
                                from e in context.Energia
                                where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date && (e.Area == Area)
                                select new
                                {
                                    fecha = e.Fecha.Value.Date,
                                    consumoTotal = e.ConsumoTotal
                                }
                            )
                            .GroupBy(g => new { g.fecha.Date })
                            .Select(s => new {
                                fecha = s.Key.Date,
                                consumo = s.Sum(t => t.consumoTotal)
                            })
                            .OrderBy(o => o.fecha.Date)
                            .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.fecha.Date.ToString("dd/MM/yyyy"),
                Valor = Decimal.Round((decimal)s.consumo, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoConsumoEnergia(DateTime FechaInicio, DateTime FechaFin)
        {
            var query = await (
                                from e in context.Energia
                                where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                                select new
                                {
                                    fecha = e.Fecha.Value.Date,
                                    consumoTotal = e.ConsumoTotal
                                }
                            )
                            .GroupBy(g => new { g.fecha.Date })
                            .Select(s => new {
                                fecha = s.Key.Date,
                                consumo = s.Sum(t => t.consumoTotal)
                            })
                            .OrderBy(o => o.fecha.Date)
                            .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.fecha.Date.ToString("dd/MM/yyyy"),
                Valor = Decimal.Round((decimal)s.consumo, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoConsumoEnergiaHora(DateTime FechaInicio, DateTime FechaFin, string Area)
        {
            var query = await (
                            from e in context.Energia
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date && (e.Area == Area)
                            select new
                            {
                                hora = e.Fecha.Value.Hour,
                                consumoTotal = e.ConsumoTotal
                            }
                        )
                        .GroupBy(g => g.hora)
                        .Select(s => new {
                            hora = s.Key,
                            consumo = s.Sum(t => t.consumoTotal)
                        })
                        .OrderBy(o => o.hora)
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.hora.ToString(),
                Valor = Decimal.Round((decimal)s.consumo, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetGraficoConsumoEnergiaHora(DateTime FechaInicio, DateTime FechaFin)
        {
            var query = await(
                            from e in context.Energia
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                            select new
                            {
                                hora = e.Fecha.Value.Hour,
                                consumoTotal = e.ConsumoTotal
                            }
                        )
                        .GroupBy(g => g.hora)
                        .Select(s => new {
                            hora = s.Key,
                            consumo = s.Sum(t => t.consumoTotal)
                        })
                        .OrderBy(o => o.hora)
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.hora.ToString(),
                Valor = Decimal.Round((decimal)s.consumo, 2)
            });

            return resultado;
        }

        public async Task<IEnumerable<GraficaDTO>> GetPorcenjateConsumo(DateTime FechaInicio, DateTime FechaFin)
        {
            var consumoTotal = await(
                 from e in context.Energia
                 where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                 select new
                 {
                     consumo = e.ConsumoTotal,
                 }
                ).SumAsync(s => s.consumo);

            var query = await(
                            from e in context.Energia
                            where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                            select new
                            {
                                consumo = e.ConsumoTotal,
                                area = e.Area
                            }
                        )
                        .GroupBy(g => new { g.area })
                        .Select(s => new {
                            porcentaje = s.Sum(t => t.consumo) * 100 / consumoTotal,
                            area = s.Key.area
                        })
                        .ToListAsync();

            var resultado = query.Select(s => new GraficaDTO
            {
                Etiqueta = s.area,
                Valor = Decimal.Round((decimal)s.porcentaje, 2)
            });

            return resultado;
        }

        public async Task<IndicadoresEnergiaDTO> GetIndicadores(DateTime FechaInicio, DateTime FechaFin)
        {
            var consumoEnergia = await (from e in context.Energia
                                        where e.Fecha.Value.Date >= FechaInicio.Date && e.Fecha.Value.Date <= FechaFin.Date
                                        select new
                                        {
                                            totalEnergia = e.ConsumoTotal
                                        }
                                        )
                                        .SumAsync(s => s.totalEnergia);

            var cajasUnidad = await (from p in context.Produccion
                                     where p.Fecha.Date >= FechaInicio.Date && p.Fecha.Date <= FechaFin.Date
                                     select new
                                     {
                                         totalCajasUnidad = p.CajasUnidad
                                     }
                                        )
                                        .SumAsync(s => s.totalCajasUnidad);

            var consumoLitrosBebida = await (from p in context.Produccion
                                             where p.Fecha.Date >= FechaInicio.Date && p.Fecha.Date <= FechaFin.Date
                                             select new
                                             {
                                                 totalLitrosBebida = p.LitrosBebida
                                             }
                                        )
                                        .SumAsync(s => s.totalLitrosBebida);

            var indicadorEE = consumoLitrosBebida / consumoEnergia;

            var indicadores = new IndicadoresEnergiaDTO
            {
                ConsumoEnergia = Decimal.Round((decimal)consumoEnergia, 2),
                CajasUnidad = Decimal.Round((decimal)cajasUnidad, 2),
                ConsumoLitrosBebida = Decimal.Round((decimal)consumoLitrosBebida, 2),
                IndicadorEE = Decimal.Round((decimal)indicadorEE, 2)
            };

            return indicadores;
        }
    }
}
