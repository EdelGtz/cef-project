﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.DTOs
{
    public class IndicadoresAguaDTO
    {
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ConsumoAgua { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CajasUnidad { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ConsumoLitrosBebida { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal IndicadorRendimientoAgua { get; set; }
    }
}
