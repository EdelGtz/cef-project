﻿using cef_backend.Core.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.Interfaces
{
    public interface IEnergia
    {
        public Task<List<EnergiaDTO>> GetEnergiaData(DateTime FechaInicio, DateTime FechaFin, string Area);
        public Task<List<EnergiaDTO>> GetEnergiaData(DateTime FechaInicio, DateTime FechaFin);
        public Task<List<EnergiaAreasDTO>> GetAreas();
        public Task<IEnumerable<GraficaDTO>> GetGraficoRendimientoEnergia(DateTime FechaInicio, DateTime FechaFin);
        public Task<IEnumerable<GraficaDTO>> GetGraficoConsumoEnergia(DateTime FechaInicio, DateTime FechaFin, string Area);
        public Task<IEnumerable<GraficaDTO>> GetGraficoConsumoEnergia(DateTime FechaInicio, DateTime FechaFin);
        public Task<IEnumerable<GraficaDTO>> GetGraficoConsumoEnergiaHora(DateTime FechaInicio, DateTime FechaFin, string Area);
        public Task<IEnumerable<GraficaDTO>> GetGraficoConsumoEnergiaHora(DateTime FechaInicio, DateTime FechaFin);
        public Task<IEnumerable<GraficaDTO>> GetPorcenjateConsumo(DateTime FechaInicio, DateTime FechaFin);
        public Task<IndicadoresEnergiaDTO> GetIndicadores(DateTime FechaInicio, DateTime FechaFin);
    }
}
