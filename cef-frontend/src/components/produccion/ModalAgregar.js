import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Button, Grid, TextField } from '@mui/material';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { es } from "date-fns/locale";
import DatePicker from '@mui/lab/DatePicker';
import moment from 'moment';
import 'moment/locale/es';
import { LocalizationProvider } from '@mui/lab';
import SaveIcon from '@mui/icons-material/Save';
import { produccionAddNew, produccionClearActive, produccionStartUpdate } from '../../actions/produccion';
import { useDispatch, useSelector } from 'react-redux';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 350,
  height: 300,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const initialState = {
  cajasUnidad: 0,
  fecha: moment(Date.now())
}

export default function ModalAgregar({ openModal, setOpenModal }) {

  moment.locale('es-mx');

  const { active } = useSelector(state => state.produccion);

  const dispatch = useDispatch();

  const handleClose = () => {
    //console.log('close')
    setOpenModal(false);
    dispatch( produccionClearActive() )
  }

  const [formValues, setFormValues] = useState( initialState );

  const { cajasUnidad, fecha } = formValues;

  useEffect(() => {
    if (active) {
        let dateString = active[0].fecha.toString();
        dateString = dateString.substr(3, 2)+"/"+dateString.substr(0, 2)+"/"+dateString.substr(6, 4);
        setFormValues({
          cajasUnidad: active[0].cajasUnidad,
          fecha: moment(dateString).format()
        });
    } else {
        setFormValues(initialState);
    }        
  }, [active, setFormValues])

  const handleInputChange = ({ target }) => {
    setFormValues({
        ...formValues,
        [ target.name ]: target.value
    });
  }

  const handleSave = () => {

    if (active) {
      const values = {
        id: active[0].id,
        cajasUnidad,
        fecha
      }
      
      dispatch( produccionStartUpdate( values ) );      
    } else {
      const values = {
        cajasUnidad,
        fecha
      }
        
      dispatch( produccionAddNew( values ) );  
    }

    setOpenModal(false);
  }

  return (
    <div>
      <Modal
        open={ openModal }
        onClose={ handleClose }
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h5">
            { 
              active
              ?
                <Typography variant="h5">
                    Editar
                </Typography>
              :
                <Typography variant="h5">
                    Agregar
                </Typography>
            }
          </Typography>
          
          <Grid container spacing={3} sx={{ mt:0.1 }}>
            <Grid item xs={ 10 }>
                <TextField
                    id="cajasUnidad"
                    name="cajasUnidad"
                    value={ cajasUnidad }
                    onChange={ handleInputChange }
                    label="Cajas unidad"
                    size="small"
                    sx={{ width:'100%', ml:2 }}
                />
            </Grid>

            <Grid item xs={ 10 } sx={{ ml:2 }}>
                <LocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
                    <DatePicker
                        label="Fecha"
                        value={fecha}
                        onChange={(newValue) => {
                          setFormValues( {...formValues, fecha:newValue} );
                        }}
                        renderInput={(params) => <TextField {...params} size="small" />}
                    />
                </LocalizationProvider>
            </Grid>

            <Grid item xs={ 10 } sx={{ ml:15, mt:1 }}>
                <Button
                    color="primary"
                    onClick={ handleSave }
                    //loading={loading}
                    //loadingPosition="start"
                    startIcon={<SaveIcon />}
                    variant="contained"
                >
                    Guardar
                </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </div>
  );
}
