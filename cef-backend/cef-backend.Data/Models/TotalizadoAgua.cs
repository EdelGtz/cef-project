﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Data.Models
{
    public class TotalizadoAgua
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "decimal(18, 3)")]
        public decimal? ConsumoTotal { get; set; }
        public int? DiaSemanaNumero { get; set; }
        [MaxLength(100)]
        public string? Area { get; set; }
        public DateTime? Fecha { get; set; }
        public DateTime? FechaReporte { get; set; }
        public int Orden { get; set; }
    }
}
