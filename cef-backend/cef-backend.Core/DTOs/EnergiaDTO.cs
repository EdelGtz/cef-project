﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.DTOs
{
    public class EnergiaDTO
    {
        public string? Fecha { get; set; }
        public string DiaSemana{ get; set; }
        public decimal? Kilowatt { get; set; }
        public decimal? Corriente { get; set; }
        public decimal? Voltaje { get; set; }
        public decimal? FactorPotencia { get; set; }
        public string? Area { get; set; }
        public decimal? ConsumoTotal { get; set; }
    }
}
