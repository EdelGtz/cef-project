import { types } from "../types/types";

const initialState = {
    table: [],
    active: null,
    refresh: false,
    graficoProduccion: [],
}

export const prodReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        case types.produccionStartLoad:
            return {
                ...state,
                table: [ ...action.payload ]
            }
        case types.produccionClearData:
            return {
                ...initialState
            }
        case types.produccionSetActive:
            return {
                ...state,
                active: state.table.filter(
                    e => ( e.id === action.payload )
                )
            }
        case types.produccionClearActive:
            return {
                ...state,
                active: null
            }
        case types.produccionStartRefresh:
            return {
                ...state,
                refresh: true
            }

        case types.produccionFinishRefresh:
            return {
                ...state,
                refresh: false
            }

        case types.produccionGraficoProduccion:
            return {
                ...state,
                graficoProduccion: [ ...action.payload ]
            }
    
        default:
            return state;
    }

}