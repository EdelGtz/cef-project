import { types } from "../types/types";

const initialState = {
    table: [],
    indicadores: null,
    rendimiento: [],
    consumo: [],
    consumoHora: [],
    porcenjateConsumo: [],
}

export const energiaReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        case types.energiaTableStartLoad:
            return {
                ...state,
                table: [ ...action.payload ]
            }

        case types.energiaGraficaRendimientoEnergia:
            return {
                ...state,
                rendimiento: [ ...action.payload ]
            }

        case types.energiaGraficoConsumoEnergia:
            return {
                ...state,
                consumo: [ ...action.payload ]
            }

        case types.energiaGraficoConsumoEnergiaHora:
            return {
                ...state,
                consumoHora: [ ...action.payload ]
            }

        case types.energiaGraficoPorcentajeConsumo:
            return {
                ...state,
                porcenjateConsumo: [ ...action.payload ]
            }

        case types.energiaClearTableData:
            return {
                ...initialState
            }

        case types.energiaIndicadoresStartLoad:
            return {
                ...state,
                indicadores: action.payload
            }
    
        default:
            return state;
    }

}