﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.DTOs
{
    public class AguaDTO
    {
        public string Fecha { get; set; }
        public string DiaSemana { get; set; }
        public string Area { get; set; }
        public decimal ConsumoTotal { get; set; }
    }
}
