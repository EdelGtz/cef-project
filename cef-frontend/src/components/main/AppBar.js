import React, { useEffect, useState } from 'react'
import Toolbar from '@mui/material/Toolbar';

import Typography from '@mui/material/Typography';

import LogoutIcon from '@mui/icons-material/Logout';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Avatar, Menu, MenuItem } from '@mui/material';
import { stringAvatar } from '../../helpers/stringAvatar';
import { useKeycloak } from '@react-keycloak/web';


export const MainAppBar = ({ open, handleDrawerOpen, AppBar }) => {

    const { keycloak } = useKeycloak();

    const [anchorEl, setAnchorEl] = useState(null);
    const openMenu = Boolean(anchorEl);

    const [userName, setUserName] = useState("Usuario usuario")

    useEffect(() => {
      async function loadUserProfile (){
        const profile = await keycloak.loadUserProfile();        
        setUserName( [profile.firstName, profile.lastName].join(' ') );
      }

      loadUserProfile();
    }, [keycloak])

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        keycloak.logout();
    };

    return (
        <AppBar position="fixed" open={open} style={{ background: '#202020' }}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            Compañía Embotelladora Del Fuerte
          </Typography>

          <div style={{marginLeft: "auto", marginRight: 0}}>
                    <Avatar {...stringAvatar( userName )} onClick={handleClick} />
                    <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={openMenu}
                        onClose={handleClose}
                        MenuListProps={{
                        'aria-labelledby': 'basic-button',
                        }}
                    >
                        <MenuItem onClick={handleLogout}>
                            <LogoutIcon />
                            Logout
                        </MenuItem>
                    </Menu>
                </div>
        </Toolbar>
      </AppBar>
    )
}
