import { Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import ReactECharts from "echarts-for-react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Filtros } from "./Filtros";
import { useSelector } from "react-redux";

const initialStateGraficoRendimiento = {
  factorEE: [],
  dias: [],
};

const initialStateGraficoConsumo = {
  consumoKwh: [],
  diasConsumo: [],
};

const initialStateGraficoConsumoHora = {
  consumoPorHora: [],
  horas: [],
};

const initialStateGraficoProduccion = {
  produccion: [],
  diasProducidos: [],
};

const initialStateGraficoPorcenjate = {
  data: []
};

export const DashboardEnergiaScreen = () => {
  const { indicadores, rendimiento, consumo, consumoHora, porcenjateConsumo } = useSelector(
    (state) => state.energia
  );
  const { graficoProduccion } = useSelector((state) => state.produccion);

  const [graficoRendimiento, setGraficoRendimiento] = useState(
    initialStateGraficoRendimiento
  );
  const [graficoConsumo, setGraficoConsumo] = useState(
    initialStateGraficoConsumo
  );
  const [graficoConsumoHora, setGraficoConsumoHora] = useState(
    initialStateGraficoConsumoHora
  );
  const [graficoProduccionBebida, setGraficoProduccionBebida] = useState(
    initialStateGraficoProduccion
  );
  const [graficoPorcenjate, setGraficoPorcentaje] = useState(
    initialStateGraficoPorcenjate
  );

  const { factorEE, dias } = graficoRendimiento;
  const { consumoKwh, diasConsumo } = graficoConsumo;
  const { consumoPorHora, horas } = graficoConsumoHora;
  const { produccion, diasProducidos } = graficoProduccionBebida;
  const { data } = graficoPorcenjate;

  useEffect(() => {
    setGraficoRendimiento({
      factorEE: rendimiento.map((x) => {
        return x.valor;
      }),
      dias: rendimiento.map((x) => {
        return x.etiqueta;
      }),
    });

    setGraficoConsumo({
      consumoKwh: consumo.map((x) => {
        return x.valor;
      }),
      diasConsumo: consumo.map((x) => {
        return x.etiqueta;
      }),
    });

    setGraficoConsumoHora({
      consumoPorHora: consumoHora.map((x) => {
        return x.valor;
      }),
      hora: consumoHora.map((x) => {
        return x.etiqueta;
      }),
    });

    setGraficoProduccionBebida({
      produccion: graficoProduccion.map((x) => {
        return x.valor;
      }),
      diasProducidos: graficoProduccion.map((x) => {
        return x.etiqueta;
      }),
    });

    setGraficoPorcentaje({
      data: porcenjateConsumo.map((x) => {
        return {
          value: x.valor,
          name: x.etiqueta
        }        
      }),
    });
  }, [rendimiento, consumo, consumoHora, graficoProduccion, porcenjateConsumo]);

  let optionsGraficoRedimiento = {
    title: {
      text: "Factor EE",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Factor EE"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: dias,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Factor EE",
        type: "bar",
        data: factorEE,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
      {
        name: "Objetivo",
        type: "line",
        data: [32.75],
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
    ],
  };

  let optionsGraficoConsumo = {
    title: {
      text: "Consumo de energía en kWh",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Consumo de energía en kWh"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: diasConsumo,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Consumo de energía en kWh",
        type: "bar",
        data: consumoKwh,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
    ],
  };

  let optionsGraficoConsumoHora = {
    title: {
      text: "Consumo de energía eléctrica por hora",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Consumo por hora kWh"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: horas,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Consumo por hora kWh",
        type: "line",
        data: consumoPorHora,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
    ],
  };

  let optionsGraficoProduccion = {
    title: {
      text: "Producción de litros de bebida por día",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: "axis",
    },
    // legend: {
    //   data: ["Producción de litros de bebida"],
    // },
    toolbox: {
      show: true,
      feature: {
        //dataView: { show: true, readOnly: true },
        magicType: { show: true, type: ["line", "bar"] },
        //restore: { show: true },
        saveAsImage: { show: true },
      },
    },
    calculable: true,
    xAxis: [
      {
        type: "category",
        // prettier-ignore
        data: diasProducidos,
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Producción de litros de bebida",
        type: "bar",
        data: produccion,
        markPoint: {
          data: [
            { type: "max", name: "Max" },
            { type: "min", name: "Min" },
          ],
        },
        markLine: {
          data: [{ type: "average", name: "Avg" }],
        },
      },
    ],
  };

  let optionsGraficoPorcentaje = {
    title: {
      text: "Porcentaje de consumo por transformador",
      //subtext: 'Fake Data'
    },
    tooltip: {
      trigger: 'item'
    },
    toolbox: {
      show: true,
      feature: {
        saveAsImage: { show: true },
      },
    },
    legend: {
      top: '8%',
      left: 'center'
    },
    series: [
      {
        name: 'Transformador',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        label: {
          show: false,
          position: 'center'
        },
        emphasis: {
          label: {
            show: true,
            fontSize: '15',
            fontWeight: 'bold'
          }
        },
        labelLine: {
          show: false
        },
        data: data
      }
    ]
  };

  return (
    <div>
      <h3>Análisis de consumo de energía</h3>

      <Filtros />

      <Grid
        container
        spacing={0}
        alignItems="center"
        justifyContent="center"
        sx={{ textAlign: "center" }}
      >
        <Grid item>
          <Card sx={{ minWidth: 160 }} elevation={0}>
            <CardContent>
              <Typography color="text.primary">Factor EE</Typography>
              <Typography variant="h6" component="div">
                {indicadores != null ? indicadores.indicadorEE : 0}
              </Typography>
            </CardContent>
          </Card>
        </Grid>

        <Grid item>
          <Card sx={{ minWidth: 160 }} elevation={0}>
            <CardContent>
              <Typography color="text.primary">
                Consumo total de energía
              </Typography>
              <Typography variant="h6" component="div">
                {indicadores != null ? indicadores.consumoEnergia : 0} kWh
              </Typography>
            </CardContent>
          </Card>
        </Grid>

        <Grid item>
          <Card sx={{ minWidth: 160 }} elevation={0}>
            <CardContent>
              <Typography color="text.primary">
                Total de cajas unidad
              </Typography>
              <Typography variant="h6" component="div">
                {indicadores != null ? indicadores.cajasUnidad : 0}
              </Typography>
            </CardContent>
          </Card>
        </Grid>

        <Grid item>
          <Card sx={{ minWidth: 160 }} elevation={0}>
            <CardContent>
              <Typography color="text.primary">
                Total de litros de bebida
              </Typography>
              <Typography variant="h6" component="div">
                {indicadores != null ? indicadores.consumoLitrosBebida : 0}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={6}>
          <ReactECharts
            option={optionsGraficoRedimiento}
            style={{ height: 400 }}
          />
        </Grid>

        <Grid item xs={6}>
          <ReactECharts
            option={optionsGraficoConsumo}
            style={{ height: 400 }}
          />
        </Grid>

        <Grid item xs={6}>
          <ReactECharts
            option={optionsGraficoProduccion}
            style={{ height: 400 }}
          />
        </Grid>

        <Grid item xs={6}>
          <ReactECharts
            option={optionsGraficoPorcentaje}
            style={{ height: 400 }}
          />
        </Grid>

        <Grid item xs={12}>
          <ReactECharts
            option={optionsGraficoConsumoHora}
            style={{ height: 400 }}
          />
        </Grid>
      </Grid>
    </div>
  );
};
