﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using cef_backend.Data;

namespace cef_backend.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20211210171336_FactorEE")]
    partial class FactorEE
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.12")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("cef_backend.Data.Models.Energia", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Area")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<decimal?>("ConsumoAcumulado")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal?>("ConsumoTotal")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal?>("Corriente")
                        .HasColumnType("decimal(18,2)");

                    b.Property<int>("DiaSemananumero")
                        .HasColumnType("int");

                    b.Property<decimal?>("FactorPotencia")
                        .HasColumnType("decimal(18,2)");

                    b.Property<DateTime?>("Fecha")
                        .HasColumnType("datetime2");

                    b.Property<bool?>("HoraPunta")
                        .HasColumnType("bit");

                    b.Property<decimal?>("Kilowatt")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal?>("Voltaje")
                        .HasColumnType("decimal(18,2)");

                    b.HasKey("Id");

                    b.ToTable("Energia");
                });

            modelBuilder.Entity("cef_backend.Data.Models.Produccion", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<decimal>("CajasUnidad")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal>("FactorEE")
                        .HasColumnType("decimal(18,2)");

                    b.Property<DateTime>("Fecha")
                        .HasColumnType("datetime2");

                    b.Property<decimal>("LitrosBebida")
                        .HasColumnType("decimal(18,2)");

                    b.HasKey("Id");

                    b.ToTable("Produccion");
                });
#pragma warning restore 612, 618
        }
    }
}
