﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Core.DTOs
{
    public class ProduccionAgregarDTO
    {
        [Required]
        public decimal CajasUnidad { get; set; }
        public DateTime Fecha { get; set; }
    }
}
