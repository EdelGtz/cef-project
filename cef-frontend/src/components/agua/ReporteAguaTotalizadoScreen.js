import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Grid, IconButton } from "@mui/material";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import LoopIcon from "@mui/icons-material/Loop";
import moment from "moment";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import { es } from "date-fns/locale";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import useSWR from "swr";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { excelExport } from "../../helpers/excelExport";
import { fetchConToken } from "../../helpers/fetch";
import { finishLoading, startLoading } from "../../actions/ui";
import Swal from "sweetalert2";

moment.locale("es-mx");

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    //backgroundColor: theme.palette.common.black,
    backgroundColor: "#202020",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

let i = 1;

export const ReporteAguaTotalizadoScreen = () => {
  const dispatch = useDispatch();

  const [fecha, setFecha] = useState(moment(Date.now()).subtract(1, "days"));

  const { data: table } = useSWR(
    `/agua/getTotalizadoAgua?Fecha=${moment(fecha).format("YYYY-MM-DD")}`,
    {
      refreshInterval: 1000,
    }
  );

  const handleExcelExport = () => {
    let heading = [["Fecha", "Area", "Consumo Total"]];
    let headers = ["fecha", "area", "consumoTotal"];
    excelExport(
      heading,
      headers,
      table,
      `REPORTE DE CONSUMO DE AGUA TOTALIZADO ${moment(Date.now()).format(
        "DD-MM-YYYY"
      )}`
    );
  };

  async function handleGetGatewayData() {
    dispatch(startLoading());
    const resp = await fetchConToken("agua/getDataFromGateway", "GET");
    if (resp.ok) {
        Swal.fire(`Se actualizaron los datos`, "", "success");
    } else{
        Swal.fire(`Error al obtener los datos`, "", "error");
    }
    dispatch(finishLoading());
  }

  return (
    <>
      <Grid container>
        <Grid item xs={10}>
          <h3>Reporte de consumo de agua totalizado</h3>
        </Grid>
        <Grid item xs={2}>
          <div style={{ marginTop: 15, float: "right" }}>
            <IconButton
              color="primary"
              aria-label="FileDownload"
              aria-controls="basic-menu"
              aria-haspopup="true"
              //aria-expanded={open ? "true" : undefined}
              onClick={handleExcelExport}
            >
              <FileDownloadIcon />
            </IconButton>
          </div>
        </Grid>
      </Grid>

      <Grid container spacing={2} sx={{ mb: 1 }}>
        <Grid item>
          <LocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
            <DatePicker
              label="Fecha"
              value={fecha}
              onChange={(newValue) => {
                setFecha(moment(newValue));
              }}
              renderInput={(params) => <TextField {...params} size="small" />}
            />
          </LocalizationProvider>
        </Grid>

        <Grid item>
          <IconButton
            aria-label="search"
            sx={{
              padding: 0,
              "& svg": {
                fontSize: 30,
              },
            }}
            onClick={handleGetGatewayData}
          >
            <LoopIcon />
          </IconButton>
        </Grid>
      </Grid>

      {table && (
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 600 }} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Fecha</StyledTableCell>
                <StyledTableCell align="left">Area</StyledTableCell>
                <StyledTableCell align="right">
                  Consumo Total en Metros Cúbicos
                </StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {table.map((row) => (
                <StyledTableRow key={i++}>
                  <StyledTableCell component="th" scope="row">
                    {row.fecha}
                  </StyledTableCell>
                  <StyledTableCell align="left">{row.area}</StyledTableCell>
                  <StyledTableCell align="right">
                    {row.consumoTotal}
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </>
  );
};
