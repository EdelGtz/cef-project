﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cef_backend.Data.Models
{
    public class Energia
    {
        public int Id { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Kilowatt { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Corriente { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Voltaje { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? ConsumoTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? ConsumoAcumulado { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? FactorPotencia { get; set; }
        public DateTime? Fecha { get; set; }
        public int DiaSemananumero { get; set; }
        public bool? HoraPunta { get; set; }
        [MaxLength(50)]
        public string? Area { get; set; }
    }
}
