export const types = {
    authLogin: '[Auth] Login',
    authLogout: '[Auth] Logout',
    authChecking: '[auth] Checking login state',
    authCheckingFinish: '[auth] Finish checking login state',
    authStartLogin: '[auth] Start login',    
    authStartTokenRenew: '[auth] Start token renew',

    uiSetError: '[UI] Set error',
    uiRemoveError: '[UI] Remove error',
    uiStartLoading: '[UI] Start loading',
    uiFinishLoading: '[UI] Finish loading',

    energiaTableStartLoad: '[energia] Start loading',
    energiaClearTableData: '[energia] Clear table data',
    energiaIndicadoresStartLoad: '[energia] Indicadores start loading',
    energiaGraficaRendimientoEnergia: '[energia] Rendimiento start loading',
    energiaGraficoConsumoEnergia: '[energia] Consumo start loading',
    energiaGraficoConsumoEnergiaHora: '[energia] Consumo hora start loading',
    energiaGraficoPorcentajeConsumo: '[energia] Porcentaje consumo start loading',

    aguaTableStartLoad: '[agua] Start loading agua',
    aguaClearTableData: '[agua] Clear table data agua',
    aguaIndicadoresStartLoad: '[agua] Indicadores start loading agua',
    aguaGraficaRendimientoAgua: '[agua] Rendimiento start loading agua',
    aguaGraficoConsumoAgua: '[agua] Consumo start loading agua',
    aguaGraficoConsumoAguaHora: '[agua] Consumo hora start loading agua',
    aguaGraficoPorcentajeConsumo: '[agua] Porcentaje consumo start loading agua',

    produccionStartLoad: '[prod] Start loading',
    produccionClearData: '[prod] Clear table data',
    produccionSetActive: '[prod] Set active',
    produccionClearActive: '[prod] Clear active',
    produccionStartUpdate: '[prod] Update register',
    produccionStartRefresh: '[prod] Refresh data on changes',
    produccionFinishRefresh: '[prod] Finish refresh data on changes',
    produccionGraficoProduccion: '[prod] Produccion start loading',

}