import Swal from "sweetalert2";
import { fetchConToken } from "../helpers/fetch";
import { types } from "../types/types";
import { finishLoading, startLoading } from "./ui";

export const produccionStartLoading = ( fechaInicio, fechaFin ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`produccion/${fechaInicio}/${fechaFin}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( produccionStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const produccionStartLoad = ( values ) => ({
    type: types.produccionStartLoad,
    payload: values
})

export const clearTableData = () => ({
    type: types.produccionClearData
})

export const produccionAddNew = ( values ) => {
    return async( dispatch ) => {

        dispatch( startLoading() );

        try {
            const resp = await fetchConToken('produccion', values, 'POST');
            const body = await resp;
            
            if ( body.ok ) {
                Swal.fire('Operación exitosa', 'Registro agregado con éxito', 'success')
                dispatch( finishLoading() );
                dispatch( startRefresh() );
                dispatch( finishRefresh() );
            } else {
                Swal.fire('Error', 'Error al agregar el registro', 'error')
                dispatch( finishLoading() );
            }
            
        } catch (error) {        
            dispatch( finishLoading() );
            //console.log(error);
        }

    }
}

export const produccionStartUpdate = ( values ) => {
    return async( dispatch ) => {

        dispatch( startLoading() );

        try {            

            const resp = await fetchConToken( `produccion/${ values.id }`, values, 'PUT' );
            const body = await resp;
            //console.log(body)
            if ( body.ok ) {
                Swal.fire('Operación exitosa', 'Registro actualizado con éxito', 'success')
                dispatch( finishLoading() );
                dispatch( startRefresh() );
                dispatch( finishRefresh() );
            } else {
                Swal.fire('Error', 'Error al actualizar el registro', 'error')
                dispatch( finishLoading() );
            }
            
        } catch (error) {        
            dispatch( finishLoading() );    
            //console.log(error);
        }

    }
}

export const produccionSetActive = ( id ) => ({
    type: types.produccionSetActive,
    payload: id
})

export const produccionClearActive = () => ({
    type: types.produccionClearActive
})

const startRefresh = () => ({
    type: types.produccionStartRefresh
})

const finishRefresh = () => ({
    type: types.produccionFinishRefresh
})

/* DATOS PARA GRAFICO DE PRODUCCION DE LITROS DE BEBIDA */
export const produccionGraficoProduccionStartLoading = ( fechaInicio, fechaFin ) => {
    return async( dispatch ) => {
        try {
            dispatch(startLoading())
            const resp = await fetchConToken(`energia/graficoProduccion/${fechaInicio}/${fechaFin}`, );
            const body = await resp.json();
            
            if (body.length !== 0) {
                //console.log(body);
                dispatch( produccionGraficoProduccionStartLoad(body) );
            } else{
                dispatch(clearTableData());
                Swal.fire('No hay registros', 'No se encontraron registros con esa fecha o area', 'warning')
            }
            
        } catch (error) {
            
        }
        finally{
            dispatch(finishLoading())
        }
    }
}

const produccionGraficoProduccionStartLoad = ( values ) => ({
    type: types.produccionGraficoProduccion,
    payload: values
})