import { ReactKeycloakProvider } from '@react-keycloak/web';
import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import reportWebVitals from './reportWebVitals';
import keycloak from '../src/helpers/keycloak'

import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';


const eventLogger = (event, error) => {
  //console.log("onKeycloakEvent", event, error);
};

const tokenLogger = (tokens) => {
  //console.log("onKeycloakTokens", tokens);
};

ReactDOM.render(
  <ReactKeycloakProvider
    LoadingComponent={<Backdrop
      sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={true}
    >
      <CircularProgress color="inherit" />
    </Backdrop>}
    authClient={keycloak}
    onEvent={eventLogger}
    onTokens={tokenLogger}>
    <App />
  </ReactKeycloakProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
