﻿using cef_backend.Core.DTOs;
using cef_backend.Core.ExtensionMethods;
using cef_backend.Core.Interfaces;
using cef_backend.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cef_backend.Controllers
{
    [Route("api/agua")]
    [ApiController]
    public class AguaController : ControllerBase
    {
        private readonly IAgua agua;
        private readonly IProduccion produccion;

        public AguaController( IAgua agua, IProduccion produccion)
        {
            this.agua = agua;
            this.produccion = produccion;
        }

        [HttpGet("getReporteAgua/{FechaInicio}/{FechaFin}/{Area?}")]
        public async Task<ActionResult<AguaDTO>> GetReporteAgua(DateTime FechaInicio, DateTime FechaFin, string? Area = null)
        {
            if (String.IsNullOrEmpty(Area) || Area == "null" || Area == "--- TODAS ---")
            {
                IEnumerable<AguaDTO> resultado = await agua.GetReporteAgua(FechaInicio, FechaFin);

                return Ok(resultado);
            }
            else
            {
                IEnumerable<AguaDTO> resultado = await agua.GetReporteAgua(FechaInicio, FechaFin, Area);

                return Ok(resultado);
            }
        }

        [HttpGet("getTotalizadoAgua")]
        public async Task<ActionResult<TotalizadoAguaDTO>> GetTotalizadoAgua([FromQuery] DateTime Fecha)
        {
            try
            {
                IEnumerable<TotalizadoAguaDTO> resultado = await agua.GetTotalizadoAgua(Fecha);

                return Ok(resultado);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("areas")]
        public async Task<ActionResult<EnergiaAreasDTO>> GetAreas()
        {
            List<AguaAreasDTO> consulta = await agua.GetAreas();
            return Ok(consulta);
        }

        [HttpGet("getGraficoRendimientoAgua/{FechaInicio}/{FechaFin}/{Area?}")]
        public async Task<ActionResult<GraficaDTO>> GetGraficoRendimientoAgua(DateTime FechaInicio, DateTime FechaFin, string? Area = null)
        {
            if (string.IsNullOrEmpty(Area) || Area == "--- TODAS ---")
            {
                IEnumerable<GraficaDTO> resultado = await agua.GetGraficoRendimientoAgua(FechaInicio, FechaFin);

                return Ok(resultado);
            }
            else
            {
                IEnumerable<GraficaDTO> resultado = await agua.GetGraficoRendimientoAgua(FechaInicio, FechaFin, Area);

                return Ok(resultado);
            }
        }

        [HttpGet("getGraficoConsumoAgua/{FechaInicio}/{FechaFin}/{Area?}")]
        public async Task<ActionResult<GraficaDTO>> GetGraficoConsumoAgua(DateTime FechaInicio, DateTime FechaFin, string? Area = null)
        {
            if (string.IsNullOrEmpty(Area) || Area == "--- TODAS ---")
            {
                IEnumerable<GraficaDTO> resultado = await agua.GetGraficoConsumoAgua(FechaInicio, FechaFin);

                return Ok(resultado);
            }
            else
            {
                IEnumerable<GraficaDTO> resultado = await agua.GetGraficoConsumoAgua(FechaInicio, FechaFin, Area);

                return Ok(resultado);
            }
        }

        [HttpGet("getGraficoConsumoAguaHora/{FechaInicio}/{FechaFin}/{Area?}")]
        public async Task<ActionResult<GraficaDTO>> GetGraficoConsumoAguaHora(DateTime FechaInicio, DateTime FechaFin, string? Area = null)
        {
            if (string.IsNullOrEmpty(Area) || Area == "--- TODAS ---")
            {
                IEnumerable<GraficaDTO> resultado = await agua.GetGraficoConsumoAguaHora(FechaInicio, FechaFin);

                return Ok(resultado);
            }
            else
            {
                IEnumerable<GraficaDTO> resultado = await agua.GetGraficoConsumoAguaHora(FechaInicio, FechaFin, Area);

                return Ok(resultado);
            }

        }

        [HttpGet("graficoProduccion/{FechaInicio}/{FechaFin}")]
        public async Task<ActionResult<GraficaDTO>> GetGraficoProduccion(DateTime FechaInicio, DateTime FechaFin)
        {
            IEnumerable<GraficaDTO> resultado = await produccion.GetGraficoProduccion(FechaInicio, FechaFin);

            return Ok(resultado);
        }

        [HttpGet("getGraficoPorcentajeConsumo/{FechaInicio}/{FechaFin}")]
        public async Task<ActionResult<GraficaDTO>> GetPorcenjateConsumo(DateTime FechaInicio, DateTime FechaFin)
        {
            IEnumerable<GraficaDTO> resultado = await agua.GetPorcenjateConsumo(FechaInicio, FechaFin);

            return Ok(resultado);
        }

        [HttpGet("indicadores/{FechaInicio}/{FechaFin}")]
        public async Task<ActionResult<IndicadoresEnergiaDTO>> GetIndicadores(DateTime FechaInicio, DateTime FechaFin)
        {
            var indicadores = await agua.GetIndicadores(FechaInicio, FechaFin);

            return Ok(indicadores);
        }

        [HttpGet("getDataFromGateway")]
        public async Task<ActionResult> GetDataFromGateway()
        {
            bool consulta = await agua.GetDataFromGateway();

            if (consulta)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
