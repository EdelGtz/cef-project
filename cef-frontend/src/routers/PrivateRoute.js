import { useKeycloak } from '@react-keycloak/web';
import { Navigate } from 'react-router-dom';



export const PrivateRoute = ({ children }) => {

    const { keycloak, initialized } = useKeycloak();
    //const { uid } = useSelector(state => state.auth);
        
    return initialized && keycloak?.authenticated
        ? children
        : <Navigate to="/auth/login" />
}