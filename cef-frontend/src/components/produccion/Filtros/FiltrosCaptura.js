import React, { useEffect, useState } from 'react'
import { Grid, IconButton } from '@mui/material'
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { es } from "date-fns/locale";
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import SearchIcon from '@mui/icons-material/Search';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import Swal from 'sweetalert2';
import { produccionStartLoading } from '../../../actions/produccion';


export const FiltrosCaptura = () => {

    const dispatch = useDispatch();

    const { refresh } = useSelector(state => state.produccion)

    const [fechaInicio, setFechaInicio] = useState(moment(Date.now()));
    const [fechaFin, setFechaFin] = useState(moment(Date.now()));

    useEffect(() => {
        //console.log('buscar')
        if (fechaInicio === null || fechaFin === null) {
            Swal.fire('Error', 'Debe seleccionar una fecha de inicio y una fecha final', 'error')
        } else {
            dispatch( produccionStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD')
                ));
        }

        if ( refresh ) {
            console.log('refrescar')
            dispatch( produccionStartLoading(
                moment(fechaInicio).format('YYYY-MM-DD'),
                moment(fechaFin).format('YYYY-MM-DD')
            ));
        }
    }, [dispatch, fechaFin, fechaInicio, refresh])

    const handleSearch = () => {
        if (fechaInicio === null || fechaFin === null) {
            Swal.fire('Error', 'Debe seleccionar una fecha de inicio y una fecha final', 'error')
        } else {
            dispatch( produccionStartLoading(
                    moment(fechaInicio).format('YYYY-MM-DD'),
                    moment(fechaFin).format('YYYY-MM-DD')
                ));
        }

    }
    
    return (
        <>
            <Grid container spacing={2} sx={{ mb: 1 }}>
                <Grid item >
                    <LocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
                        <DatePicker
                            label="Fecha Inicio"
                            value={fechaInicio}
                            onChange={(newValue) => {
                                setFechaInicio(newValue);
                            }}
                            renderInput={(params) => <TextField {...params} size="small" />}
                        />
                    </LocalizationProvider>
                </Grid>

                <Grid item>
                    <LocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
                        <DatePicker
                            label="Fecha Fin"
                            value={fechaFin}
                            onChange={(newValue) => {
                                setFechaFin(newValue);
                            }}
                            renderInput={(params) => <TextField {...params} size="small" />}
                        />
                    </LocalizationProvider>
                </Grid>

                <Grid item>
                    <IconButton 
                        aria-label="search"
                        sx={{
                            padding: 0, '& svg': {
                                fontSize: 30
                            }
                        }}
                        onClick={ handleSearch }
                    >
                        <SearchIcon />
                    </IconButton>
                </Grid>
            </Grid>
        </>
    )
}
